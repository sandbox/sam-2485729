<?php

/**
 * @file
 * Contains Drupal\theme_builder\ClassGenerator.
 */

namespace Drupal\theme_builder;

use Doctrine\Common\Inflector\Inflector;
use Drupal\Core\Render\ElementInfoManagerInterface;
use Drupal\Core\Theme\Registry;

/**
 * A service to generate builder pattern classes from theme definitions.
 */
class ClassGenerator implements ClassGeneratorInterface {

  /**
   * The element info manager.
   *
   * @var \Drupal\Core\Render\ElementInfoManagerInterface
   */
  protected $renderElementManager;

  /**
   * Theme registry.
   *
   * @var \Drupal\Core\Theme\Registry
   */
  protected $registry;

  /**
   * {@inheritdoc}
   */
  public function __construct(ElementInfoManagerInterface $render_element_manager, Registry $registry) {
    $this->renderElementManager = $render_element_manager;
    $this->registry = $registry;
  }

  /**
   * {@inheritdoc}
   */
  public function generateBase($builder_key, $module) {

    // @todo, I am still looking at the best way to extract the most meaningful
    // information out of theme and element definitions. The below logic seems
    // too convoluted to be correct, but it has proven to create the most
    // methods from the available information.

    $render_element_methods = FALSE;
    $variables_methods = FALSE;

    // If there is a valid render element with the given key, extract
    // methods from it.
    $render_element = $this->renderElementManager->getInfo($builder_key);
    $render_element = count($render_element) > 1 ? $render_element : FALSE;
    if ($render_element) {
      $render_element_methods = $this->methodNamesFromRenderElement($builder_key);
    }
    // Otherwise gain information from a theme defintion with the given key.
    elseif ($definition = $this->getThemeDefinition($builder_key)) {
      // Theme definitons using the 'variables' key.
      if (isset($definition['variables'])) {
        $variables_methods = $this->methodNamesFromVariables(array_keys($definition['variables']));
      }
      // Theme definitons using the 'render element' key.
      if (isset($definition['render element'])) {
        $render_element_methods = $this->methodNamesFromRenderElement($definition['render element']);
      }
    }
    else {
      throw new \Exception(sprintf("The builder key %s did not have a corresponding theme definition or render element.", $builder_key));
    }

    return [
      '#theme' => 'builder_class_base',
      '#theme_name' => $builder_key,
      '#methods' => $variables_methods,
      '#render_element_methods' => $render_element_methods,
      '#module' => $module,
      '#class_name' => Inflector::classify($builder_key . 'Builder'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function generateStub($builder_key, $module) {
    // The stub requires the same information as the base, but with a different
    // template.
    $stub = $this->generateBase($builder_key, $module);
    $stub['#theme'] = 'builder_class_stub';
    return $stub;
  }

  /**
   * Return a list of methods keyed by the variables they represent.
   *
   * Variables which have underscores will be converted to camel-case, for
   * example list_type will become listType.
   *
   * @param $variable_names
   *   An array of variables.
   *
   * @return array
   *   An array of method names keyed by variable name.
   */
  protected function methodNamesFromVariables($variable_names) {
    $methods = [];
    foreach ($variable_names as $variable_name) {
      $methods[$variable_name] = Inflector::camelize('set_' . $variable_name);
    }
    return $methods;
  }

  /**
   * Extract method names from a render element.
   *
   * @param string $render_element
   *   A Drupal render element.
   *
   * @return array
   *   Methods keyed by render element key.
   */
  protected function methodNamesFromRenderElement($render_element) {
    $render_element_info = $this->renderElementManager->getInfo($render_element);
    // Include default methods.
    $methods = [];
    foreach ($render_element_info as $key => $default) {
      $render_element_key = ltrim($key, '#');
      $methods[$render_element_key] = Inflector::camelize('set_' . $render_element_key);
    }
    // Exclude some methods from the getInfo call of the render element.
    $exclusions = ['theme', 'type', 'defaults_loaded'];
    foreach ($exclusions as $exclusion) {
      if (isset($methods[$exclusion])) {
        unset($methods[$exclusion]);
      }
    }
    return $methods;
  }

  /**
   * Get a definition from the registry by name.
   *
   * @param $name
   *   The name of the theme definition.
   * @return bool|array
   *   A theme definition or FALSE if not found.
   */
  protected function getThemeDefinition($name) {
    $defs = $this->registry->get();
    return $defs[$name] ?: FALSE;
  }
}
