<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FilterGuidelinesBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\FilterGuidelinesBuilderBase;

/**
 * The filter_guidelines theme builder.
 */
class FilterGuidelinesBuilder extends FilterGuidelinesBuilderBase {
}
