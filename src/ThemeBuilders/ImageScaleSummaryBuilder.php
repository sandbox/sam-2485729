<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ImageScaleSummaryBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\ImageScaleSummaryBuilderBase;

/**
 * The image_scale_summary theme builder.
 */
class ImageScaleSummaryBuilder extends ImageScaleSummaryBuilderBase {
}
