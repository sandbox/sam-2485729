<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FieldUiTableBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\FieldUiTableBuilderBase;

/**
 * The field_ui_table theme builder.
 */
class FieldUiTableBuilder extends FieldUiTableBuilderBase {
}
