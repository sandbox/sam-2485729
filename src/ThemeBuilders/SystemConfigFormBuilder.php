<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\SystemConfigFormBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\SystemConfigFormBuilderBase;

/**
 * The system_config_form theme builder.
 */
class SystemConfigFormBuilder extends SystemConfigFormBuilderBase {
}
