<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\BlockContentAddListBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\BlockContentAddListBuilderBase;

/**
 * The block_content_add_list theme builder.
 */
class BlockContentAddListBuilder extends BlockContentAddListBuilderBase {
}
