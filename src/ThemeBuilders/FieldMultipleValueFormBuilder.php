<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FieldMultipleValueFormBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\FieldMultipleValueFormBuilderBase;

/**
 * The field_multiple_value_form theme builder.
 */
class FieldMultipleValueFormBuilder extends FieldMultipleValueFormBuilderBase {
}
