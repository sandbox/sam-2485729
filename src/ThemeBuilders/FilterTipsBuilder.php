<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FilterTipsBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\FilterTipsBuilderBase;

/**
 * The filter_tips theme builder.
 */
class FilterTipsBuilder extends FilterTipsBuilderBase {
}
