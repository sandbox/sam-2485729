<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FieldBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\FieldBuilderBase;

/**
 * The field theme builder.
 */
class FieldBuilder extends FieldBuilderBase {
}
