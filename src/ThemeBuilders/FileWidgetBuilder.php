<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FileWidgetBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\FileWidgetBuilderBase;

/**
 * The file_widget theme builder.
 */
class FileWidgetBuilder extends FileWidgetBuilderBase {
}
