<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\AuthorizeMessageBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\AuthorizeMessageBuilderBase;

/**
 * The authorize_message theme builder.
 */
class AuthorizeMessageBuilder extends AuthorizeMessageBuilderBase {
}
