<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\PasswordBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\PasswordBuilderBase;

/**
 * The password theme builder.
 */
class PasswordBuilder extends PasswordBuilderBase {
}
