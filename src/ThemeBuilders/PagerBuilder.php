<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\PagerBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\PagerBuilderBase;

/**
 * The pager theme builder.
 */
class PagerBuilder extends PagerBuilderBase {
}
