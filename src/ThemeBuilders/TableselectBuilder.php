<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\TableselectBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\TableselectBuilderBase;

/**
 * The tableselect theme builder.
 */
class TableselectBuilder extends TableselectBuilderBase {
}
