<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\TaxonomyTermBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\TaxonomyTermBuilderBase;

/**
 * The taxonomy_term theme builder.
 */
class TaxonomyTermBuilder extends TaxonomyTermBuilderBase {
}
