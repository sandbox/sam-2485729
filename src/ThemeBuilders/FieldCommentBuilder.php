<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FieldCommentBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\FieldCommentBuilderBase;

/**
 * The field__comment theme builder.
 */
class FieldCommentBuilder extends FieldCommentBuilderBase {
}
