<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FileWidgetMultipleBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\FileWidgetMultipleBuilderBase;

/**
 * The file_widget_multiple theme builder.
 */
class FileWidgetMultipleBuilder extends FileWidgetMultipleBuilderBase {
}
