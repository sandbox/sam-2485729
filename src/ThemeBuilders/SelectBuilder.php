<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\SelectBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\BuilderHelpers\FormElementTrait;
use Drupal\theme_builder\ThemeBuilders\generated\SelectBuilderBase;

/**
 * The select theme builder.
 */
class SelectBuilder extends SelectBuilderBase {

  use FormElementTrait;

}
