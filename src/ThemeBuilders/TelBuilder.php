<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\TelBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\TelBuilderBase;

/**
 * The tel theme builder.
 */
class TelBuilder extends TelBuilderBase {
}
