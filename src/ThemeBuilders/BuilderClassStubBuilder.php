<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\BuilderClassStubBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\BuilderClassStubBuilderBase;

/**
 * The builder_class_stub theme builder.
 */
class BuilderClassStubBuilder extends BuilderClassStubBuilderBase {
}
