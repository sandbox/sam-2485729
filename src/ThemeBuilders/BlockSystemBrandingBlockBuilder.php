<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\BlockSystemBrandingBlockBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\BlockSystemBrandingBlockBuilderBase;

/**
 * The block__system_branding_block theme builder.
 */
class BlockSystemBrandingBlockBuilder extends BlockSystemBrandingBlockBuilderBase {
}
