<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\LinkFormatterLinkSeparateBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\LinkFormatterLinkSeparateBuilderBase;

/**
 * The link_formatter_link_separate theme builder.
 */
class LinkFormatterLinkSeparateBuilder extends LinkFormatterLinkSeparateBuilderBase {
}
