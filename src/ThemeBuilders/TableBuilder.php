<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\TableBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\TableBuilderBase;

/**
 * The table theme builder.
 */
class TableBuilder extends TableBuilderBase {
}
