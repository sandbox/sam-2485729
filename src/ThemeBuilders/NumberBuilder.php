<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\NumberBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\NumberBuilderBase;

/**
 * The number theme builder.
 */
class NumberBuilder extends NumberBuilderBase {
}
