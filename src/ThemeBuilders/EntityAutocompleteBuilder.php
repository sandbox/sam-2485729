<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\EntityAutocompleteBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\EntityAutocompleteBuilderBase;

/**
 * The entity_autocomplete theme builder.
 */
class EntityAutocompleteBuilder extends EntityAutocompleteBuilderBase {
}
