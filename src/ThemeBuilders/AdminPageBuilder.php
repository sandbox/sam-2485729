<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\AdminPageBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\AdminPageBuilderBase;

/**
 * The admin_page theme builder.
 */
class AdminPageBuilder extends AdminPageBuilderBase {
}
