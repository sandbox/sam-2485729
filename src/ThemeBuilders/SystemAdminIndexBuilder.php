<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\SystemAdminIndexBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\SystemAdminIndexBuilderBase;

/**
 * The system_admin_index theme builder.
 */
class SystemAdminIndexBuilder extends SystemAdminIndexBuilderBase {
}
