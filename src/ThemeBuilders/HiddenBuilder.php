<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\HiddenBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\HiddenBuilderBase;

/**
 * The hidden theme builder.
 */
class HiddenBuilder extends HiddenBuilderBase {
}
