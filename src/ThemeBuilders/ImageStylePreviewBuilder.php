<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ImageStylePreviewBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\ImageStylePreviewBuilderBase;

/**
 * The image_style_preview theme builder.
 */
class ImageStylePreviewBuilder extends ImageStylePreviewBuilderBase {
}
