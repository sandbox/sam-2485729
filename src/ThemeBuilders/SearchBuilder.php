<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\SearchBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\SearchBuilderBase;

/**
 * The search theme builder.
 */
class SearchBuilder extends SearchBuilderBase {
}
