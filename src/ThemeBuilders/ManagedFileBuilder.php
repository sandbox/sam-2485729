<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ManagedFileBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\ManagedFileBuilderBase;

/**
 * The managed_file theme builder.
 */
class ManagedFileBuilder extends ManagedFileBuilderBase {
}
