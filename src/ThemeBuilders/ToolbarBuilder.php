<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ToolbarBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\ToolbarBuilderBase;

/**
 * The toolbar theme builder.
 */
class ToolbarBuilder extends ToolbarBuilderBase {
}
