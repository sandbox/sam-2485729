<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\BlockBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\BlockBuilderBase;

/**
 * The block theme builder.
 */
class BlockBuilder extends BlockBuilderBase {
}
