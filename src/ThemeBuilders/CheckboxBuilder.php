<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\CheckboxBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\CheckboxBuilderBase;

/**
 * The checkbox theme builder.
 */
class CheckboxBuilder extends CheckboxBuilderBase {
}
