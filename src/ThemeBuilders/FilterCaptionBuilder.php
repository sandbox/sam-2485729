<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FilterCaptionBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\FilterCaptionBuilderBase;

/**
 * The filter_caption theme builder.
 */
class FilterCaptionBuilder extends FilterCaptionBuilderBase {
}
