<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\AdminBlockContentBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\AdminBlockContentBuilderBase;

/**
 * The admin_block_content theme builder.
 */
class AdminBlockContentBuilder extends AdminBlockContentBuilderBase {
}
