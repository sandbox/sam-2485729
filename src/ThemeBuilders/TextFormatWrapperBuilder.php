<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\TextFormatWrapperBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\TextFormatWrapperBuilderBase;

/**
 * The text_format_wrapper theme builder.
 */
class TextFormatWrapperBuilder extends TextFormatWrapperBuilderBase {
}
