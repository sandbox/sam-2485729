<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\DropbuttonWrapperBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\DropbuttonWrapperBuilderBase;

/**
 * The dropbutton_wrapper theme builder.
 */
class DropbuttonWrapperBuilder extends DropbuttonWrapperBuilderBase {
}
