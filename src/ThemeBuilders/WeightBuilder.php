<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\WeightBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\WeightBuilderBase;

/**
 * The weight theme builder.
 */
class WeightBuilder extends WeightBuilderBase {
}
