<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ImageCropSummaryBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\ImageCropSummaryBuilderBase;

/**
 * The image_crop_summary theme builder.
 */
class ImageCropSummaryBuilder extends ImageCropSummaryBuilderBase {
}
