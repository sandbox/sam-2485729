<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\DatetimeBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\DatetimeBuilderBase;

/**
 * The datetime theme builder.
 */
class DatetimeBuilder extends DatetimeBuilderBase {
}
