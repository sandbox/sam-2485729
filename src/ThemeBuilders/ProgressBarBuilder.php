<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ProgressBarBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\ProgressBarBuilderBase;

/**
 * The progress_bar theme builder.
 */
class ProgressBarBuilder extends ProgressBarBuilderBase {
}
