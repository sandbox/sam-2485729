<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\LanguageSelectBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\LanguageSelectBuilderBase;

/**
 * The language_select theme builder.
 */
class LanguageSelectBuilder extends LanguageSelectBuilderBase {
}
