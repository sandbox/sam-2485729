<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FieldNodeTitleBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\FieldNodeTitleBuilderBase;

/**
 * The field__node__title theme builder.
 */
class FieldNodeTitleBuilder extends FieldNodeTitleBuilderBase {
}
