<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\RadiosBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\BuilderHelpers\FormElementTrait;
use Drupal\theme_builder\ThemeBuilders\generated\RadiosBuilderBase;

/**
 * The radios theme builder.
 */
class RadiosBuilder extends RadiosBuilderBase {

  use FormElementTrait;

}
