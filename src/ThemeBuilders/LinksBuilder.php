<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\LinksBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\LinksBuilderBase;

/**
 * The links theme builder.
 */
class LinksBuilder extends LinksBuilderBase {
}
