<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\VerticalTabsBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\VerticalTabsBuilderBase;

/**
 * The vertical_tabs theme builder.
 */
class VerticalTabsBuilder extends VerticalTabsBuilderBase {
}
