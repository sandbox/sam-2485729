<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FormElementBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\FormElementBuilderBase;

/**
 * The form_element theme builder.
 */
class FormElementBuilder extends FormElementBuilderBase {
}
