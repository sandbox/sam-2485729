<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\AdminBlockBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\AdminBlockBuilderBase;

/**
 * The admin_block theme builder.
 */
class AdminBlockBuilder extends AdminBlockBuilderBase {
}
