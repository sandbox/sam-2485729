<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\TextareaBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\BuilderHelpers\FormElementTrait;
use Drupal\theme_builder\ThemeBuilders\generated\TextareaBuilderBase;

/**
 * The textarea theme builder.
 */
class TextareaBuilder extends TextareaBuilderBase {

  use FormElementTrait;

}
