<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\PathBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\PathBuilderBase;

/**
 * The path theme builder.
 */
class PathBuilder extends PathBuilderBase {
}
