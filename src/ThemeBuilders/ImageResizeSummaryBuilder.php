<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ImageResizeSummaryBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\ImageResizeSummaryBuilderBase;

/**
 * The image_resize_summary theme builder.
 */
class ImageResizeSummaryBuilder extends ImageResizeSummaryBuilderBase {
}
