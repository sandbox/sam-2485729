<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\InputBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\InputBuilderBase;

/**
 * The input theme builder.
 */
class InputBuilder extends InputBuilderBase {
}
