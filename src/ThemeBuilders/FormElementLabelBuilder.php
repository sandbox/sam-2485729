<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FormElementLabelBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\FormElementLabelBuilderBase;

/**
 * The form_element_label theme builder.
 */
class FormElementLabelBuilder extends FormElementLabelBuilderBase {
}
