<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ColorBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\ColorBuilderBase;

/**
 * The color theme builder.
 */
class ColorBuilder extends ColorBuilderBase {
}
