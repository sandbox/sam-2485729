<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\TextfieldBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\BuilderHelpers\FormElementTrait;
use Drupal\theme_builder\ThemeBuilders\generated\TextfieldBuilderBase;

/**
 * The textfield theme builder.
 */
class TextfieldBuilder extends TextfieldBuilderBase {

  use FormElementTrait;

}
