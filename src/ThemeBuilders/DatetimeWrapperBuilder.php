<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\DatetimeWrapperBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\DatetimeWrapperBuilderBase;

/**
 * The datetime_wrapper theme builder.
 */
class DatetimeWrapperBuilder extends DatetimeWrapperBuilderBase {
}
