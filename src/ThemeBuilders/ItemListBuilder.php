<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ItemListBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\BuilderHelpers\ValidationTrait;
use Drupal\theme_builder\ThemeBuilders\generated\ItemListBuilderBase;

/**
 * The item_list theme builder.
 */
class ItemListBuilder extends ItemListBuilderBase {

  use ValidationTrait;

  /**
   * {@inheritdoc}
   */
  public function setItems($value) {
    return parent::setItems($this->validateArray($value));
  }
}
