<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\RegionBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\RegionBuilderBase;

/**
 * The region theme builder.
 */
class RegionBuilder extends RegionBuilderBase {
}
