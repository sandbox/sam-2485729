<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\RangeBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\RangeBuilderBase;

/**
 * The range theme builder.
 */
class RangeBuilder extends RangeBuilderBase {
}
