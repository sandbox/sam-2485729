<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\DetailsBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\DetailsBuilderBase;

/**
 * The details theme builder.
 */
class DetailsBuilder extends DetailsBuilderBase {
}
