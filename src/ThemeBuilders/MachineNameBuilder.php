<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\MachineNameBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\MachineNameBuilderBase;

/**
 * The machine_name theme builder.
 */
class MachineNameBuilder extends MachineNameBuilderBase {
}
