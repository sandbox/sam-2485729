<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\BlockSystemMessagesBlockBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\BlockSystemMessagesBlockBuilderBase;

/**
 * The block__system_messages_block theme builder.
 */
class BlockSystemMessagesBlockBuilder extends BlockSystemMessagesBlockBuilderBase {
}
