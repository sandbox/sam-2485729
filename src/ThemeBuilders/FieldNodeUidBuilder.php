<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FieldNodeUidBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\FieldNodeUidBuilderBase;

/**
 * The field__node__uid theme builder.
 */
class FieldNodeUidBuilder extends FieldNodeUidBuilderBase {
}
