<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\BuilderClassBaseBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\BuilderClassBaseBuilderBase;

/**
 * The builder_class_base theme builder.
 */
class BuilderClassBaseBuilder extends BuilderClassBaseBuilderBase {
}
