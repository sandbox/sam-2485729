<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\NodeBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\NodeBuilderBase;

/**
 * The node theme builder.
 */
class NodeBuilder extends NodeBuilderBase {
}
