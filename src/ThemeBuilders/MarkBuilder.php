<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\MarkBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\MarkBuilderBase;

/**
 * The mark theme builder.
 */
class MarkBuilder extends MarkBuilderBase {
}
