<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\AuthorizeReportBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\AuthorizeReportBuilderBase;

/**
 * The authorize_report theme builder.
 */
class AuthorizeReportBuilder extends AuthorizeReportBuilderBase {
}
