<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ImageRotateSummaryBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\ImageRotateSummaryBuilderBase;

/**
 * The image_rotate_summary theme builder.
 */
class ImageRotateSummaryBuilder extends ImageRotateSummaryBuilderBase {
}
