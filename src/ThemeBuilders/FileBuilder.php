<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FileBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\FileBuilderBase;

/**
 * The file theme builder.
 */
class FileBuilder extends FileBuilderBase {
}
