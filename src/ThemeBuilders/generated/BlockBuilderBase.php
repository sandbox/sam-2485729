<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\BlockBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\BlockBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'block' theme builder.
 */
abstract class BlockBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'block'];

}
