<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\PageBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\PageBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'page' theme builder.
 */
abstract class PageBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'page'];

  /**
   * Set the title property on the page.
   */
  public function setTitle($value) {
    $this->renderable['#title'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the page.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

}
