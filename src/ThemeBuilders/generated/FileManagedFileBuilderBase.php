<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FileManagedFileBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\FileManagedFileBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'file_managed_file' theme builder.
 */
abstract class FileManagedFileBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'file_managed_file'];

}
