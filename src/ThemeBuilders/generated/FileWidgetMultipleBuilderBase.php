<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FileWidgetMultipleBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\FileWidgetMultipleBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'file_widget_multiple' theme builder.
 */
abstract class FileWidgetMultipleBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'file_widget_multiple'];

}
