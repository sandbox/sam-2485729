<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\TaxonomyTermBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\TaxonomyTermBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'taxonomy_term' theme builder.
 */
abstract class TaxonomyTermBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'taxonomy_term'];

}
