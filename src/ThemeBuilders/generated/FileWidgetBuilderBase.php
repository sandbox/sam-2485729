<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FileWidgetBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\FileWidgetBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'file_widget' theme builder.
 */
abstract class FileWidgetBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'file_widget'];

}
