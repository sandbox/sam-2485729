<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ImageWidgetBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\ImageWidgetBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'image_widget' theme builder.
 */
abstract class ImageWidgetBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'image_widget'];

}
