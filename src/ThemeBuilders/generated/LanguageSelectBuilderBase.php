<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\LanguageSelectBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\LanguageSelectBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'language_select' theme builder.
 */
abstract class LanguageSelectBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'language_select'];

  /**
   * Set the input property on the language_select.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the default_value property on the language_select.
   */
  public function setDefaultValue($value) {
    $this->renderable['#default_value'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the language_select.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
