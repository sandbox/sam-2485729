<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ToolbarBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\ToolbarBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'toolbar' theme builder.
 */
abstract class ToolbarBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'toolbar'];

  /**
   * Set the pre_render property on the toolbar.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the attached property on the toolbar.
   */
  public function setAttached($value) {
    $this->renderable['#attached'] = $value;
    return $this;
  }

  /**
   * Set the attributes property on the toolbar.
   */
  public function setAttributes($value) {
    $this->renderable['#attributes'] = $value;
    return $this;
  }

  /**
   * Set the bar property on the toolbar.
   */
  public function setBar($value) {
    $this->renderable['#bar'] = $value;
    return $this;
  }

}
