<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FileUploadHelpBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\FileUploadHelpBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'file_upload_help' theme builder.
 */
abstract class FileUploadHelpBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'file_upload_help'];

  /**
   * Set the description property on the file_upload_help.
   */
  public function setDescription($value) {
    $this->renderable['#description'] = $value;
    return $this;
  }
  /**
   * Set the upload_validators property on the file_upload_help.
   */
  public function setUploadValidators($value) {
    $this->renderable['#upload_validators'] = $value;
    return $this;
  }
  /**
   * Set the cardinality property on the file_upload_help.
   */
  public function setCardinality($value) {
    $this->renderable['#cardinality'] = $value;
    return $this;
  }
}
