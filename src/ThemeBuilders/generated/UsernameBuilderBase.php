<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\UsernameBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\UsernameBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'username' theme builder.
 */
abstract class UsernameBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'username'];

  /**
   * Set the account property on the username.
   */
  public function setAccount($value) {
    $this->renderable['#account'] = $value;
    return $this;
  }
  /**
   * Set the attributes property on the username.
   */
  public function setAttributes($value) {
    $this->renderable['#attributes'] = $value;
    return $this;
  }
  /**
   * Set the link_options property on the username.
   */
  public function setLinkOptions($value) {
    $this->renderable['#link_options'] = $value;
    return $this;
  }
}
