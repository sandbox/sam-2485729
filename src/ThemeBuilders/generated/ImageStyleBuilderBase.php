<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ImageStyleBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\ImageStyleBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'image_style' theme builder.
 */
abstract class ImageStyleBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'image_style'];

  /**
   * Set the style_name property on the image_style.
   */
  public function setStyleName($value) {
    $this->renderable['#style_name'] = $value;
    return $this;
  }
  /**
   * Set the uri property on the image_style.
   */
  public function setUri($value) {
    $this->renderable['#uri'] = $value;
    return $this;
  }
  /**
   * Set the width property on the image_style.
   */
  public function setWidth($value) {
    $this->renderable['#width'] = $value;
    return $this;
  }
  /**
   * Set the height property on the image_style.
   */
  public function setHeight($value) {
    $this->renderable['#height'] = $value;
    return $this;
  }
  /**
   * Set the alt property on the image_style.
   */
  public function setAlt($value) {
    $this->renderable['#alt'] = $value;
    return $this;
  }
  /**
   * Set the title property on the image_style.
   */
  public function setTitle($value) {
    $this->renderable['#title'] = $value;
    return $this;
  }
  /**
   * Set the attributes property on the image_style.
   */
  public function setAttributes($value) {
    $this->renderable['#attributes'] = $value;
    return $this;
  }
}
