<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FilterCaptionBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\FilterCaptionBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'filter_caption' theme builder.
 */
abstract class FilterCaptionBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'filter_caption'];

  /**
   * Set the node property on the filter_caption.
   */
  public function setNode($value) {
    $this->renderable['#node'] = $value;
    return $this;
  }
  /**
   * Set the tag property on the filter_caption.
   */
  public function setTag($value) {
    $this->renderable['#tag'] = $value;
    return $this;
  }
  /**
   * Set the caption property on the filter_caption.
   */
  public function setCaption($value) {
    $this->renderable['#caption'] = $value;
    return $this;
  }
  /**
   * Set the classes property on the filter_caption.
   */
  public function setClasses($value) {
    $this->renderable['#classes'] = $value;
    return $this;
  }
}
