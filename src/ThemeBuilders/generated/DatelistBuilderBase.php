<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\DatelistBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\DatelistBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'datelist' theme builder.
 */
abstract class DatelistBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'datelist'];

  /**
   * Set the input property on the datelist.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the element_validate property on the datelist.
   */
  public function setElementValidate($value) {
    $this->renderable['#element_validate'] = $value;
    return $this;
  }

  /**
   * Set the process property on the datelist.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the datelist.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

  /**
   * Set the date_part_order property on the datelist.
   */
  public function setDatePartOrder($value) {
    $this->renderable['#date_part_order'] = $value;
    return $this;
  }

  /**
   * Set the date_year_range property on the datelist.
   */
  public function setDateYearRange($value) {
    $this->renderable['#date_year_range'] = $value;
    return $this;
  }

  /**
   * Set the date_increment property on the datelist.
   */
  public function setDateIncrement($value) {
    $this->renderable['#date_increment'] = $value;
    return $this;
  }

  /**
   * Set the date_date_callbacks property on the datelist.
   */
  public function setDateDateCallbacks($value) {
    $this->renderable['#date_date_callbacks'] = $value;
    return $this;
  }

  /**
   * Set the date_timezone property on the datelist.
   */
  public function setDateTimezone($value) {
    $this->renderable['#date_timezone'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the datelist.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
