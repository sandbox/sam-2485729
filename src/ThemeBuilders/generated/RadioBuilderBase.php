<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\RadioBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\RadioBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'radio' theme builder.
 */
abstract class RadioBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'radio'];

  /**
   * Set the input property on the radio.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the default_value property on the radio.
   */
  public function setDefaultValue($value) {
    $this->renderable['#default_value'] = $value;
    return $this;
  }

  /**
   * Set the process property on the radio.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the radio.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the radio.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

  /**
   * Set the title_display property on the radio.
   */
  public function setTitleDisplay($value) {
    $this->renderable['#title_display'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the radio.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
