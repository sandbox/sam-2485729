<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\SubmitBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\SubmitBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'submit' theme builder.
 */
abstract class SubmitBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'submit'];

  /**
   * Set the executes_submit_callback property on the submit.
   */
  public function setExecutesSubmitCallback($value) {
    $this->renderable['#executes_submit_callback'] = $value;
    return $this;
  }

  /**
   * Set the input property on the submit.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the name property on the submit.
   */
  public function setName($value) {
    $this->renderable['#name'] = $value;
    return $this;
  }

  /**
   * Set the is_button property on the submit.
   */
  public function setIsButton($value) {
    $this->renderable['#is_button'] = $value;
    return $this;
  }

  /**
   * Set the limit_validation_errors property on the submit.
   */
  public function setLimitValidationErrors($value) {
    $this->renderable['#limit_validation_errors'] = $value;
    return $this;
  }

  /**
   * Set the process property on the submit.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the submit.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the submit.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the submit.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
