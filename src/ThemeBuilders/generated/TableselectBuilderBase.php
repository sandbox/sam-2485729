<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\TableselectBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\TableselectBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'tableselect' theme builder.
 */
abstract class TableselectBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'tableselect'];

  /**
   * Set the input property on the tableselect.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the js_select property on the tableselect.
   */
  public function setJsSelect($value) {
    $this->renderable['#js_select'] = $value;
    return $this;
  }

  /**
   * Set the multiple property on the tableselect.
   */
  public function setMultiple($value) {
    $this->renderable['#multiple'] = $value;
    return $this;
  }

  /**
   * Set the responsive property on the tableselect.
   */
  public function setResponsive($value) {
    $this->renderable['#responsive'] = $value;
    return $this;
  }

  /**
   * Set the sticky property on the tableselect.
   */
  public function setSticky($value) {
    $this->renderable['#sticky'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the tableselect.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the process property on the tableselect.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the options property on the tableselect.
   */
  public function setOptions($value) {
    $this->renderable['#options'] = $value;
    return $this;
  }

  /**
   * Set the empty property on the tableselect.
   */
  public function setEmpty($value) {
    $this->renderable['#empty'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the tableselect.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
