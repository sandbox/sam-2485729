<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\SearchResultBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\SearchResultBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'search_result' theme builder.
 */
abstract class SearchResultBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'search_result'];

  /**
   * Set the result property on the search_result.
   */
  public function setResult($value) {
    $this->renderable['#result'] = $value;
    return $this;
  }
  /**
   * Set the plugin_id property on the search_result.
   */
  public function setPluginId($value) {
    $this->renderable['#plugin_id'] = $value;
    return $this;
  }
}
