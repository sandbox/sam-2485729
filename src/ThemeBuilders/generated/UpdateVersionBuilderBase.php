<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\UpdateVersionBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\UpdateVersionBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'update_version' theme builder.
 */
abstract class UpdateVersionBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'update_version'];

  /**
   * Set the version property on the update_version.
   */
  public function setVersion($value) {
    $this->renderable['#version'] = $value;
    return $this;
  }
  /**
   * Set the title property on the update_version.
   */
  public function setTitle($value) {
    $this->renderable['#title'] = $value;
    return $this;
  }
  /**
   * Set the attributes property on the update_version.
   */
  public function setAttributes($value) {
    $this->renderable['#attributes'] = $value;
    return $this;
  }
}
