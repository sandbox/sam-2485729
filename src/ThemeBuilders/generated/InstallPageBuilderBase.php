<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\InstallPageBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\InstallPageBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'install_page' theme builder.
 */
abstract class InstallPageBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'install_page'];

  /**
   * Set the title property on the install_page.
   */
  public function setTitle($value) {
    $this->renderable['#title'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the install_page.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

}
