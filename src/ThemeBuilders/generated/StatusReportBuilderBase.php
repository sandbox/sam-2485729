<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\StatusReportBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\StatusReportBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'status_report' theme builder.
 */
abstract class StatusReportBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'status_report'];

  /**
   * Set the requirements property on the status_report.
   */
  public function setRequirements($value) {
    $this->renderable['#requirements'] = $value;
    return $this;
  }
}
