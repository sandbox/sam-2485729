<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ImageStylePreviewBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\ImageStylePreviewBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'image_style_preview' theme builder.
 */
abstract class ImageStylePreviewBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'image_style_preview'];

  /**
   * Set the style property on the image_style_preview.
   */
  public function setStyle($value) {
    $this->renderable['#style'] = $value;
    return $this;
  }
}
