<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\AdminPageBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\AdminPageBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'admin_page' theme builder.
 */
abstract class AdminPageBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'admin_page'];

  /**
   * Set the blocks property on the admin_page.
   */
  public function setBlocks($value) {
    $this->renderable['#blocks'] = $value;
    return $this;
  }
}
