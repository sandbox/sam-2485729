<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FieldBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\FieldBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'field' theme builder.
 */
abstract class FieldBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'field'];

}
