<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FieldCommentBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\FieldCommentBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'field__comment' theme builder.
 */
abstract class FieldCommentBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'field__comment'];

}
