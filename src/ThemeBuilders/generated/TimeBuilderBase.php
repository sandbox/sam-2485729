<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\TimeBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\TimeBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'time' theme builder.
 */
abstract class TimeBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'time'];

  /**
   * Set the timestamp property on the time.
   */
  public function setTimestamp($value) {
    $this->renderable['#timestamp'] = $value;
    return $this;
  }
  /**
   * Set the text property on the time.
   */
  public function setText($value) {
    $this->renderable['#text'] = $value;
    return $this;
  }
  /**
   * Set the attributes property on the time.
   */
  public function setAttributes($value) {
    $this->renderable['#attributes'] = $value;
    return $this;
  }
  /**
   * Set the html property on the time.
   */
  public function setHtml($value) {
    $this->renderable['#html'] = $value;
    return $this;
  }
}
