<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\WeightBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\WeightBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'weight' theme builder.
 */
abstract class WeightBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'weight'];

  /**
   * Set the input property on the weight.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the delta property on the weight.
   */
  public function setDelta($value) {
    $this->renderable['#delta'] = $value;
    return $this;
  }

  /**
   * Set the default_value property on the weight.
   */
  public function setDefaultValue($value) {
    $this->renderable['#default_value'] = $value;
    return $this;
  }

  /**
   * Set the process property on the weight.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the weight.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
