<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FormElementLabelBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\FormElementLabelBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'form_element_label' theme builder.
 */
abstract class FormElementLabelBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'form_element_label'];

}
