<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ColorSchemeFormBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\ColorSchemeFormBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'color_scheme_form' theme builder.
 */
abstract class ColorSchemeFormBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'color_scheme_form'];

  /**
   * Set the method property on the color_scheme_form.
   */
  public function setMethod($value) {
    $this->renderable['#method'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the color_scheme_form.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

}
