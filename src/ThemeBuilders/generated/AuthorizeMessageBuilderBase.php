<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\AuthorizeMessageBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\AuthorizeMessageBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'authorize_message' theme builder.
 */
abstract class AuthorizeMessageBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'authorize_message'];

  /**
   * Set the message property on the authorize_message.
   */
  public function setMessage($value) {
    $this->renderable['#message'] = $value;
    return $this;
  }
  /**
   * Set the success property on the authorize_message.
   */
  public function setSuccess($value) {
    $this->renderable['#success'] = $value;
    return $this;
  }
}
