<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\TokenBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\TokenBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'token' theme builder.
 */
abstract class TokenBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'token'];

  /**
   * Set the input property on the token.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the token.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the token.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
