<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FileLinkBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\FileLinkBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'file_link' theme builder.
 */
abstract class FileLinkBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'file_link'];

  /**
   * Set the file property on the file_link.
   */
  public function setFile($value) {
    $this->renderable['#file'] = $value;
    return $this;
  }
  /**
   * Set the description property on the file_link.
   */
  public function setDescription($value) {
    $this->renderable['#description'] = $value;
    return $this;
  }
  /**
   * Set the attributes property on the file_link.
   */
  public function setAttributes($value) {
    $this->renderable['#attributes'] = $value;
    return $this;
  }
}
