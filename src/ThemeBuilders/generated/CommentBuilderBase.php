<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\CommentBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\CommentBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'comment' theme builder.
 */
abstract class CommentBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'comment'];

}
