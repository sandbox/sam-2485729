<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\HtmlBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\HtmlBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'html' theme builder.
 */
abstract class HtmlBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'html'];

  /**
   * Set the attached property on the html.
   */
  public function setAttached($value) {
    $this->renderable['#attached'] = $value;
    return $this;
  }

}
