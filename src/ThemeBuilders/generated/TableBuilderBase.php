<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\TableBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\TableBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'table' theme builder.
 */
abstract class TableBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'table'];

  /**
   * Set the header property on the table.
   */
  public function setHeader($value) {
    $this->renderable['#header'] = $value;
    return $this;
  }

  /**
   * Set the rows property on the table.
   */
  public function setRows($value) {
    $this->renderable['#rows'] = $value;
    return $this;
  }

  /**
   * Set the empty property on the table.
   */
  public function setEmpty($value) {
    $this->renderable['#empty'] = $value;
    return $this;
  }

  /**
   * Set the input property on the table.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the tree property on the table.
   */
  public function setTree($value) {
    $this->renderable['#tree'] = $value;
    return $this;
  }

  /**
   * Set the tableselect property on the table.
   */
  public function setTableselect($value) {
    $this->renderable['#tableselect'] = $value;
    return $this;
  }

  /**
   * Set the sticky property on the table.
   */
  public function setSticky($value) {
    $this->renderable['#sticky'] = $value;
    return $this;
  }

  /**
   * Set the responsive property on the table.
   */
  public function setResponsive($value) {
    $this->renderable['#responsive'] = $value;
    return $this;
  }

  /**
   * Set the multiple property on the table.
   */
  public function setMultiple($value) {
    $this->renderable['#multiple'] = $value;
    return $this;
  }

  /**
   * Set the js_select property on the table.
   */
  public function setJsSelect($value) {
    $this->renderable['#js_select'] = $value;
    return $this;
  }

  /**
   * Set the process property on the table.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the element_validate property on the table.
   */
  public function setElementValidate($value) {
    $this->renderable['#element_validate'] = $value;
    return $this;
  }

  /**
   * Set the tabledrag property on the table.
   */
  public function setTabledrag($value) {
    $this->renderable['#tabledrag'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the table.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the table.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
