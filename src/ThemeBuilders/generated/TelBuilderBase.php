<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\TelBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\TelBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'tel' theme builder.
 */
abstract class TelBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'tel'];

  /**
   * Set the input property on the tel.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the size property on the tel.
   */
  public function setSize($value) {
    $this->renderable['#size'] = $value;
    return $this;
  }

  /**
   * Set the maxlength property on the tel.
   */
  public function setMaxlength($value) {
    $this->renderable['#maxlength'] = $value;
    return $this;
  }

  /**
   * Set the autocomplete_route_name property on the tel.
   */
  public function setAutocompleteRouteName($value) {
    $this->renderable['#autocomplete_route_name'] = $value;
    return $this;
  }

  /**
   * Set the process property on the tel.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the tel.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the tel.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the tel.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
