<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\TablesortIndicatorBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\TablesortIndicatorBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'tablesort_indicator' theme builder.
 */
abstract class TablesortIndicatorBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'tablesort_indicator'];

  /**
   * Set the style property on the tablesort_indicator.
   */
  public function setStyle($value) {
    $this->renderable['#style'] = $value;
    return $this;
  }
}
