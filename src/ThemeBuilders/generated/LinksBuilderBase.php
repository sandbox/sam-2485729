<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\LinksBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\LinksBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'links' theme builder.
 */
abstract class LinksBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'links'];

  /**
   * Set the links property on the links.
   */
  public function setLinks($value) {
    $this->renderable['#links'] = $value;
    return $this;
  }
  /**
   * Set the attributes property on the links.
   */
  public function setAttributes($value) {
    $this->renderable['#attributes'] = $value;
    return $this;
  }
  /**
   * Set the heading property on the links.
   */
  public function setHeading($value) {
    $this->renderable['#heading'] = $value;
    return $this;
  }
  /**
   * Set the set_active_class property on the links.
   */
  public function setSetActiveClass($value) {
    $this->renderable['#set_active_class'] = $value;
    return $this;
  }
}
