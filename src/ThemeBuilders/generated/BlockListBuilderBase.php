<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\BlockListBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\BlockListBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'block_list' theme builder.
 */
abstract class BlockListBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'block_list'];

  /**
   * Set the method property on the block_list.
   */
  public function setMethod($value) {
    $this->renderable['#method'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the block_list.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

}
