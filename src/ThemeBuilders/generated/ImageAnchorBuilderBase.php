<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ImageAnchorBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\ImageAnchorBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'image_anchor' theme builder.
 */
abstract class ImageAnchorBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'image_anchor'];

}
