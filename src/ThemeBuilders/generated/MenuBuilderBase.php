<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\MenuBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\MenuBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'menu' theme builder.
 */
abstract class MenuBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'menu'];

  /**
   * Set the items property on the menu.
   */
  public function setItems($value) {
    $this->renderable['#items'] = $value;
    return $this;
  }
  /**
   * Set the attributes property on the menu.
   */
  public function setAttributes($value) {
    $this->renderable['#attributes'] = $value;
    return $this;
  }
}
