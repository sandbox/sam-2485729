<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\CheckboxesBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\CheckboxesBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'checkboxes' theme builder.
 */
abstract class CheckboxesBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'checkboxes'];

  /**
   * Set the input property on the checkboxes.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the process property on the checkboxes.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the checkboxes.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the checkboxes.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the checkboxes.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
