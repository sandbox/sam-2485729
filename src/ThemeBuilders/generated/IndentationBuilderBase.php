<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\IndentationBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\IndentationBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'indentation' theme builder.
 */
abstract class IndentationBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'indentation'];

  /**
   * Set the size property on the indentation.
   */
  public function setSize($value) {
    $this->renderable['#size'] = $value;
    return $this;
  }
}
