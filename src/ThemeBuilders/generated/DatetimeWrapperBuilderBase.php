<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\DatetimeWrapperBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\DatetimeWrapperBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'datetime_wrapper' theme builder.
 */
abstract class DatetimeWrapperBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'datetime_wrapper'];

}
