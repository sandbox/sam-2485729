<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\DetailsBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\DetailsBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'details' theme builder.
 */
abstract class DetailsBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'details'];

  /**
   * Set the open property on the details.
   */
  public function setOpen($value) {
    $this->renderable['#open'] = $value;
    return $this;
  }

  /**
   * Set the value property on the details.
   */
  public function setValue($value) {
    $this->renderable['#value'] = $value;
    return $this;
  }

  /**
   * Set the process property on the details.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the details.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the details.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

}
