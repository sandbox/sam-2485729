<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\MachineNameBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\MachineNameBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'machine_name' theme builder.
 */
abstract class MachineNameBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'machine_name'];

  /**
   * Set the input property on the machine_name.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the default_value property on the machine_name.
   */
  public function setDefaultValue($value) {
    $this->renderable['#default_value'] = $value;
    return $this;
  }

  /**
   * Set the required property on the machine_name.
   */
  public function setRequired($value) {
    $this->renderable['#required'] = $value;
    return $this;
  }

  /**
   * Set the maxlength property on the machine_name.
   */
  public function setMaxlength($value) {
    $this->renderable['#maxlength'] = $value;
    return $this;
  }

  /**
   * Set the size property on the machine_name.
   */
  public function setSize($value) {
    $this->renderable['#size'] = $value;
    return $this;
  }

  /**
   * Set the autocomplete_route_name property on the machine_name.
   */
  public function setAutocompleteRouteName($value) {
    $this->renderable['#autocomplete_route_name'] = $value;
    return $this;
  }

  /**
   * Set the process property on the machine_name.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the element_validate property on the machine_name.
   */
  public function setElementValidate($value) {
    $this->renderable['#element_validate'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the machine_name.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the machine_name.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the machine_name.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
