<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\UpdateReportBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\UpdateReportBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'update_report' theme builder.
 */
abstract class UpdateReportBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'update_report'];

  /**
   * Set the data property on the update_report.
   */
  public function setData($value) {
    $this->renderable['#data'] = $value;
    return $this;
  }
}
