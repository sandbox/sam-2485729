<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FieldNodeUidBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\FieldNodeUidBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'field__node__uid' theme builder.
 */
abstract class FieldNodeUidBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'field__node__uid'];

}
