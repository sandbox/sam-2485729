<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\SystemModulesUninstallBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\SystemModulesUninstallBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'system_modules_uninstall' theme builder.
 */
abstract class SystemModulesUninstallBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'system_modules_uninstall'];

  /**
   * Set the method property on the system_modules_uninstall.
   */
  public function setMethod($value) {
    $this->renderable['#method'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the system_modules_uninstall.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

}
