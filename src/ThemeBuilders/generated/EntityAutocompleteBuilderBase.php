<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\EntityAutocompleteBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\EntityAutocompleteBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'entity_autocomplete' theme builder.
 */
abstract class EntityAutocompleteBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'entity_autocomplete'];

  /**
   * Set the input property on the entity_autocomplete.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the size property on the entity_autocomplete.
   */
  public function setSize($value) {
    $this->renderable['#size'] = $value;
    return $this;
  }

  /**
   * Set the maxlength property on the entity_autocomplete.
   */
  public function setMaxlength($value) {
    $this->renderable['#maxlength'] = $value;
    return $this;
  }

  /**
   * Set the autocomplete_route_name property on the entity_autocomplete.
   */
  public function setAutocompleteRouteName($value) {
    $this->renderable['#autocomplete_route_name'] = $value;
    return $this;
  }

  /**
   * Set the process property on the entity_autocomplete.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the entity_autocomplete.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the entity_autocomplete.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

  /**
   * Set the target_type property on the entity_autocomplete.
   */
  public function setTargetType($value) {
    $this->renderable['#target_type'] = $value;
    return $this;
  }

  /**
   * Set the selection_handler property on the entity_autocomplete.
   */
  public function setSelectionHandler($value) {
    $this->renderable['#selection_handler'] = $value;
    return $this;
  }

  /**
   * Set the selection_settings property on the entity_autocomplete.
   */
  public function setSelectionSettings($value) {
    $this->renderable['#selection_settings'] = $value;
    return $this;
  }

  /**
   * Set the tags property on the entity_autocomplete.
   */
  public function setTags($value) {
    $this->renderable['#tags'] = $value;
    return $this;
  }

  /**
   * Set the autocreate property on the entity_autocomplete.
   */
  public function setAutocreate($value) {
    $this->renderable['#autocreate'] = $value;
    return $this;
  }

  /**
   * Set the validate_reference property on the entity_autocomplete.
   */
  public function setValidateReference($value) {
    $this->renderable['#validate_reference'] = $value;
    return $this;
  }

  /**
   * Set the process_default_value property on the entity_autocomplete.
   */
  public function setProcessDefaultValue($value) {
    $this->renderable['#process_default_value'] = $value;
    return $this;
  }

  /**
   * Set the element_validate property on the entity_autocomplete.
   */
  public function setElementValidate($value) {
    $this->renderable['#element_validate'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the entity_autocomplete.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
