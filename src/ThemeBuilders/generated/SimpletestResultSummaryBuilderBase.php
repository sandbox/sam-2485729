<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\SimpletestResultSummaryBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\SimpletestResultSummaryBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'simpletest_result_summary' theme builder.
 */
abstract class SimpletestResultSummaryBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'simpletest_result_summary'];

  /**
   * Set the label property on the simpletest_result_summary.
   */
  public function setLabel($value) {
    $this->renderable['#label'] = $value;
    return $this;
  }
  /**
   * Set the items property on the simpletest_result_summary.
   */
  public function setItems($value) {
    $this->renderable['#items'] = $value;
    return $this;
  }
  /**
   * Set the pass property on the simpletest_result_summary.
   */
  public function setPass($value) {
    $this->renderable['#pass'] = $value;
    return $this;
  }
  /**
   * Set the fail property on the simpletest_result_summary.
   */
  public function setFail($value) {
    $this->renderable['#fail'] = $value;
    return $this;
  }
  /**
   * Set the exception property on the simpletest_result_summary.
   */
  public function setException($value) {
    $this->renderable['#exception'] = $value;
    return $this;
  }
  /**
   * Set the debug property on the simpletest_result_summary.
   */
  public function setDebug($value) {
    $this->renderable['#debug'] = $value;
    return $this;
  }
}
