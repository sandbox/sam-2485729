<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FieldUiTableBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\FieldUiTableBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'field_ui_table' theme builder.
 */
abstract class FieldUiTableBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'field_ui_table'];

  /**
   * Set the regions property on the field_ui_table.
   */
  public function setRegions($value) {
    $this->renderable['#regions'] = $value;
    return $this;
  }

}
