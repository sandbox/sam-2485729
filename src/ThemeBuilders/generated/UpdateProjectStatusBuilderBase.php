<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\UpdateProjectStatusBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\UpdateProjectStatusBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'update_project_status' theme builder.
 */
abstract class UpdateProjectStatusBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'update_project_status'];

  /**
   * Set the project property on the update_project_status.
   */
  public function setProject($value) {
    $this->renderable['#project'] = $value;
    return $this;
  }
  /**
   * Set the includes_status property on the update_project_status.
   */
  public function setIncludesStatus($value) {
    $this->renderable['#includes_status'] = $value;
    return $this;
  }
}
