<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\AdminBlockBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\AdminBlockBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'admin_block' theme builder.
 */
abstract class AdminBlockBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'admin_block'];

  /**
   * Set the block property on the admin_block.
   */
  public function setBlock($value) {
    $this->renderable['#block'] = $value;
    return $this;
  }
}
