<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\SearchBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\SearchBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'search' theme builder.
 */
abstract class SearchBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'search'];

  /**
   * Set the input property on the search.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the size property on the search.
   */
  public function setSize($value) {
    $this->renderable['#size'] = $value;
    return $this;
  }

  /**
   * Set the maxlength property on the search.
   */
  public function setMaxlength($value) {
    $this->renderable['#maxlength'] = $value;
    return $this;
  }

  /**
   * Set the autocomplete_route_name property on the search.
   */
  public function setAutocompleteRouteName($value) {
    $this->renderable['#autocomplete_route_name'] = $value;
    return $this;
  }

  /**
   * Set the process property on the search.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the search.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the search.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the search.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
