<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\HiddenBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\HiddenBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'hidden' theme builder.
 */
abstract class HiddenBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'hidden'];

  /**
   * Set the input property on the hidden.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the process property on the hidden.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the hidden.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the hidden.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
