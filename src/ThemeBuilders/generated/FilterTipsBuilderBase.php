<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FilterTipsBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\FilterTipsBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'filter_tips' theme builder.
 */
abstract class FilterTipsBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'filter_tips'];

  /**
   * Set the tips property on the filter_tips.
   */
  public function setTips($value) {
    $this->renderable['#tips'] = $value;
    return $this;
  }
  /**
   * Set the long property on the filter_tips.
   */
  public function setLong($value) {
    $this->renderable['#long'] = $value;
    return $this;
  }
}
