<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\NumberBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\NumberBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'number' theme builder.
 */
abstract class NumberBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'number'];

  /**
   * Set the input property on the number.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the step property on the number.
   */
  public function setStep($value) {
    $this->renderable['#step'] = $value;
    return $this;
  }

  /**
   * Set the process property on the number.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the element_validate property on the number.
   */
  public function setElementValidate($value) {
    $this->renderable['#element_validate'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the number.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the number.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the number.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
