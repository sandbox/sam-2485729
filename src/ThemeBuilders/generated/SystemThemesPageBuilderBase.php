<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\SystemThemesPageBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\SystemThemesPageBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'system_themes_page' theme builder.
 */
abstract class SystemThemesPageBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'system_themes_page'];

  /**
   * Set the theme_groups property on the system_themes_page.
   */
  public function setThemeGroups($value) {
    $this->renderable['#theme_groups'] = $value;
    return $this;
  }
  /**
   * Set the theme_group_titles property on the system_themes_page.
   */
  public function setThemeGroupTitles($value) {
    $this->renderable['#theme_group_titles'] = $value;
    return $this;
  }
}
