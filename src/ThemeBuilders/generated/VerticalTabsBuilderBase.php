<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\VerticalTabsBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\VerticalTabsBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'vertical_tabs' theme builder.
 */
abstract class VerticalTabsBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'vertical_tabs'];

  /**
   * Set the default_tab property on the vertical_tabs.
   */
  public function setDefaultTab($value) {
    $this->renderable['#default_tab'] = $value;
    return $this;
  }

  /**
   * Set the process property on the vertical_tabs.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the vertical_tabs.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the vertical_tabs.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

}
