<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\BuilderClassStubBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\BuilderClassStubBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'builder_class_stub' theme builder.
 */
abstract class BuilderClassStubBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'builder_class_stub'];

  /**
   * Set the class_name property on the builder_class_stub.
   */
  public function setClassName($value) {
    $this->renderable['#class_name'] = $value;
    return $this;
  }
  /**
   * Set the methods property on the builder_class_stub.
   */
  public function setMethods($value) {
    $this->renderable['#methods'] = $value;
    return $this;
  }
  /**
   * Set the render_element_methods property on the builder_class_stub.
   */
  public function setRenderElementMethods($value) {
    $this->renderable['#render_element_methods'] = $value;
    return $this;
  }
  /**
   * Set the theme_name property on the builder_class_stub.
   */
  public function setThemeName($value) {
    $this->renderable['#theme_name'] = $value;
    return $this;
  }
  /**
   * Set the module property on the builder_class_stub.
   */
  public function setModule($value) {
    $this->renderable['#module'] = $value;
    return $this;
  }
}
