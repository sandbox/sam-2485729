<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\BreadcrumbBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\BreadcrumbBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'breadcrumb' theme builder.
 */
abstract class BreadcrumbBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'breadcrumb'];

  /**
   * Set the links property on the breadcrumb.
   */
  public function setLinks($value) {
    $this->renderable['#links'] = $value;
    return $this;
  }
}
