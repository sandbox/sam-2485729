<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FileBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\FileBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'file' theme builder.
 */
abstract class FileBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'file'];

  /**
   * Set the input property on the file.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the multiple property on the file.
   */
  public function setMultiple($value) {
    $this->renderable['#multiple'] = $value;
    return $this;
  }

  /**
   * Set the process property on the file.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the size property on the file.
   */
  public function setSize($value) {
    $this->renderable['#size'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the file.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the file.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the file.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
