<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\TextareaBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\TextareaBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'textarea' theme builder.
 */
abstract class TextareaBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'textarea'];

  /**
   * Set the input property on the textarea.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the cols property on the textarea.
   */
  public function setCols($value) {
    $this->renderable['#cols'] = $value;
    return $this;
  }

  /**
   * Set the rows property on the textarea.
   */
  public function setRows($value) {
    $this->renderable['#rows'] = $value;
    return $this;
  }

  /**
   * Set the resizable property on the textarea.
   */
  public function setResizable($value) {
    $this->renderable['#resizable'] = $value;
    return $this;
  }

  /**
   * Set the process property on the textarea.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the textarea.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the textarea.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the textarea.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
