<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\BlockSystemBrandingBlockBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\BlockSystemBrandingBlockBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'block__system_branding_block' theme builder.
 */
abstract class BlockSystemBrandingBlockBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'block__system_branding_block'];

}
