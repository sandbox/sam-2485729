<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\MaintenanceTaskListBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\MaintenanceTaskListBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'maintenance_task_list' theme builder.
 */
abstract class MaintenanceTaskListBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'maintenance_task_list'];

  /**
   * Set the items property on the maintenance_task_list.
   */
  public function setItems($value) {
    $this->renderable['#items'] = $value;
    return $this;
  }
  /**
   * Set the active property on the maintenance_task_list.
   */
  public function setActive($value) {
    $this->renderable['#active'] = $value;
    return $this;
  }
  /**
   * Set the variant property on the maintenance_task_list.
   */
  public function setVariant($value) {
    $this->renderable['#variant'] = $value;
    return $this;
  }
}
