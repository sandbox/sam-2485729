<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\RangeBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\RangeBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'range' theme builder.
 */
abstract class RangeBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'range'];

  /**
   * Set the min property on the range.
   */
  public function setMin($value) {
    $this->renderable['#min'] = $value;
    return $this;
  }

  /**
   * Set the max property on the range.
   */
  public function setMax($value) {
    $this->renderable['#max'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the range.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the input property on the range.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the step property on the range.
   */
  public function setStep($value) {
    $this->renderable['#step'] = $value;
    return $this;
  }

  /**
   * Set the process property on the range.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the element_validate property on the range.
   */
  public function setElementValidate($value) {
    $this->renderable['#element_validate'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the range.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the range.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
