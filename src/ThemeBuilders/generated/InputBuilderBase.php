<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\InputBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\InputBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'input' theme builder.
 */
abstract class InputBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'input'];

}
