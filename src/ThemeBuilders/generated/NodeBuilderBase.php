<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\NodeBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\NodeBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'node' theme builder.
 */
abstract class NodeBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'node'];

}
