<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\BuilderClassBaseBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\BuilderClassBaseBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'builder_class_base' theme builder.
 */
abstract class BuilderClassBaseBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'builder_class_base'];

  /**
   * Set the class_name property on the builder_class_base.
   */
  public function setClassName($value) {
    $this->renderable['#class_name'] = $value;
    return $this;
  }
  /**
   * Set the methods property on the builder_class_base.
   */
  public function setMethods($value) {
    $this->renderable['#methods'] = $value;
    return $this;
  }
  /**
   * Set the render_element_methods property on the builder_class_base.
   */
  public function setRenderElementMethods($value) {
    $this->renderable['#render_element_methods'] = $value;
    return $this;
  }
  /**
   * Set the theme_name property on the builder_class_base.
   */
  public function setThemeName($value) {
    $this->renderable['#theme_name'] = $value;
    return $this;
  }
  /**
   * Set the module property on the builder_class_base.
   */
  public function setModule($value) {
    $this->renderable['#module'] = $value;
    return $this;
  }
}
