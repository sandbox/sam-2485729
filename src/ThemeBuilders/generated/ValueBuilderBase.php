<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ValueBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\ValueBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'value' theme builder.
 */
abstract class ValueBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'value'];

  /**
   * Set the input property on the value.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the value.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
