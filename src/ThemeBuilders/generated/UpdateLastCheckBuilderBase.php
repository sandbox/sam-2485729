<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\UpdateLastCheckBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\UpdateLastCheckBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'update_last_check' theme builder.
 */
abstract class UpdateLastCheckBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'update_last_check'];

  /**
   * Set the last property on the update_last_check.
   */
  public function setLast($value) {
    $this->renderable['#last'] = $value;
    return $this;
  }
}
