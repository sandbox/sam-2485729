<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\DropbuttonWrapperBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\DropbuttonWrapperBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'dropbutton_wrapper' theme builder.
 */
abstract class DropbuttonWrapperBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'dropbutton_wrapper'];

  /**
   * Set the children property on the dropbutton_wrapper.
   */
  public function setChildren($value) {
    $this->renderable['#children'] = $value;
    return $this;
  }
}
