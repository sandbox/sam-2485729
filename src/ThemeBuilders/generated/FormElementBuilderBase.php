<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FormElementBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\FormElementBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'form_element' theme builder.
 */
abstract class FormElementBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'form_element'];

}
