<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FilterGuidelinesBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\FilterGuidelinesBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'filter_guidelines' theme builder.
 */
abstract class FilterGuidelinesBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'filter_guidelines'];

  /**
   * Set the format property on the filter_guidelines.
   */
  public function setFormat($value) {
    $this->renderable['#format'] = $value;
    return $this;
  }
}
