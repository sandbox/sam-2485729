<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FieldMultipleValueFormBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\FieldMultipleValueFormBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'field_multiple_value_form' theme builder.
 */
abstract class FieldMultipleValueFormBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'field_multiple_value_form'];

}
