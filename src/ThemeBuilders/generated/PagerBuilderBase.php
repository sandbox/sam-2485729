<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\PagerBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\PagerBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'pager' theme builder.
 */
abstract class PagerBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'pager'];

  /**
   * Set the pre_render property on the pager.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the element property on the pager.
   */
  public function setElement($value) {
    $this->renderable['#element'] = $value;
    return $this;
  }

  /**
   * Set the parameters property on the pager.
   */
  public function setParameters($value) {
    $this->renderable['#parameters'] = $value;
    return $this;
  }

  /**
   * Set the quantity property on the pager.
   */
  public function setQuantity($value) {
    $this->renderable['#quantity'] = $value;
    return $this;
  }

  /**
   * Set the tags property on the pager.
   */
  public function setTags($value) {
    $this->renderable['#tags'] = $value;
    return $this;
  }

}
