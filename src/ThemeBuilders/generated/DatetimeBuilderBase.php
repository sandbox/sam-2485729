<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\DatetimeBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\DatetimeBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'datetime' theme builder.
 */
abstract class DatetimeBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'datetime'];

  /**
   * Set the input property on the datetime.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the element_validate property on the datetime.
   */
  public function setElementValidate($value) {
    $this->renderable['#element_validate'] = $value;
    return $this;
  }

  /**
   * Set the process property on the datetime.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the datetime.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the datetime.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

  /**
   * Set the date_date_format property on the datetime.
   */
  public function setDateDateFormat($value) {
    $this->renderable['#date_date_format'] = $value;
    return $this;
  }

  /**
   * Set the date_date_element property on the datetime.
   */
  public function setDateDateElement($value) {
    $this->renderable['#date_date_element'] = $value;
    return $this;
  }

  /**
   * Set the date_date_callbacks property on the datetime.
   */
  public function setDateDateCallbacks($value) {
    $this->renderable['#date_date_callbacks'] = $value;
    return $this;
  }

  /**
   * Set the date_time_format property on the datetime.
   */
  public function setDateTimeFormat($value) {
    $this->renderable['#date_time_format'] = $value;
    return $this;
  }

  /**
   * Set the date_time_element property on the datetime.
   */
  public function setDateTimeElement($value) {
    $this->renderable['#date_time_element'] = $value;
    return $this;
  }

  /**
   * Set the date_time_callbacks property on the datetime.
   */
  public function setDateTimeCallbacks($value) {
    $this->renderable['#date_time_callbacks'] = $value;
    return $this;
  }

  /**
   * Set the date_year_range property on the datetime.
   */
  public function setDateYearRange($value) {
    $this->renderable['#date_year_range'] = $value;
    return $this;
  }

  /**
   * Set the date_increment property on the datetime.
   */
  public function setDateIncrement($value) {
    $this->renderable['#date_increment'] = $value;
    return $this;
  }

  /**
   * Set the date_timezone property on the datetime.
   */
  public function setDateTimezone($value) {
    $this->renderable['#date_timezone'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the datetime.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
