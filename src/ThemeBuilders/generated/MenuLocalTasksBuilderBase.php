<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\MenuLocalTasksBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\MenuLocalTasksBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'menu_local_tasks' theme builder.
 */
abstract class MenuLocalTasksBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'menu_local_tasks'];

  /**
   * Set the primary property on the menu_local_tasks.
   */
  public function setPrimary($value) {
    $this->renderable['#primary'] = $value;
    return $this;
  }
  /**
   * Set the secondary property on the menu_local_tasks.
   */
  public function setSecondary($value) {
    $this->renderable['#secondary'] = $value;
    return $this;
  }
}
