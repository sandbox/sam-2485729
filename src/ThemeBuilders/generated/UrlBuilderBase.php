<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\UrlBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\UrlBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'url' theme builder.
 */
abstract class UrlBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'url'];

  /**
   * Set the input property on the url.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the size property on the url.
   */
  public function setSize($value) {
    $this->renderable['#size'] = $value;
    return $this;
  }

  /**
   * Set the maxlength property on the url.
   */
  public function setMaxlength($value) {
    $this->renderable['#maxlength'] = $value;
    return $this;
  }

  /**
   * Set the autocomplete_route_name property on the url.
   */
  public function setAutocompleteRouteName($value) {
    $this->renderable['#autocomplete_route_name'] = $value;
    return $this;
  }

  /**
   * Set the process property on the url.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the element_validate property on the url.
   */
  public function setElementValidate($value) {
    $this->renderable['#element_validate'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the url.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the url.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the url.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
