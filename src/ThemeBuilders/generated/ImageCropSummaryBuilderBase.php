<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ImageCropSummaryBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\ImageCropSummaryBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'image_crop_summary' theme builder.
 */
abstract class ImageCropSummaryBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'image_crop_summary'];

  /**
   * Set the data property on the image_crop_summary.
   */
  public function setData($value) {
    $this->renderable['#data'] = $value;
    return $this;
  }
  /**
   * Set the effect property on the image_crop_summary.
   */
  public function setEffect($value) {
    $this->renderable['#effect'] = $value;
    return $this;
  }
}
