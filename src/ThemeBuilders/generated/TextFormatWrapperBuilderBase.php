<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\TextFormatWrapperBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\TextFormatWrapperBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'text_format_wrapper' theme builder.
 */
abstract class TextFormatWrapperBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'text_format_wrapper'];

  /**
   * Set the children property on the text_format_wrapper.
   */
  public function setChildren($value) {
    $this->renderable['#children'] = $value;
    return $this;
  }
  /**
   * Set the description property on the text_format_wrapper.
   */
  public function setDescription($value) {
    $this->renderable['#description'] = $value;
    return $this;
  }
  /**
   * Set the attributes property on the text_format_wrapper.
   */
  public function setAttributes($value) {
    $this->renderable['#attributes'] = $value;
    return $this;
  }
}
