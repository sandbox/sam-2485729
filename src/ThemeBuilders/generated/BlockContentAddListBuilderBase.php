<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\BlockContentAddListBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\BlockContentAddListBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'block_content_add_list' theme builder.
 */
abstract class BlockContentAddListBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'block_content_add_list'];

  /**
   * Set the content property on the block_content_add_list.
   */
  public function setContent($value) {
    $this->renderable['#content'] = $value;
    return $this;
  }
}
