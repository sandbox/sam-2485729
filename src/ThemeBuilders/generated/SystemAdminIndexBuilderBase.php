<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\SystemAdminIndexBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\SystemAdminIndexBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'system_admin_index' theme builder.
 */
abstract class SystemAdminIndexBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'system_admin_index'];

  /**
   * Set the menu_items property on the system_admin_index.
   */
  public function setMenuItems($value) {
    $this->renderable['#menu_items'] = $value;
    return $this;
  }
}
