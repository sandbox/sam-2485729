<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ImageFormatterBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\ImageFormatterBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'image_formatter' theme builder.
 */
abstract class ImageFormatterBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'image_formatter'];

  /**
   * Set the item property on the image_formatter.
   */
  public function setItem($value) {
    $this->renderable['#item'] = $value;
    return $this;
  }
  /**
   * Set the item_attributes property on the image_formatter.
   */
  public function setItemAttributes($value) {
    $this->renderable['#item_attributes'] = $value;
    return $this;
  }
  /**
   * Set the url property on the image_formatter.
   */
  public function setUrl($value) {
    $this->renderable['#url'] = $value;
    return $this;
  }
  /**
   * Set the image_style property on the image_formatter.
   */
  public function setImageStyle($value) {
    $this->renderable['#image_style'] = $value;
    return $this;
  }
}
