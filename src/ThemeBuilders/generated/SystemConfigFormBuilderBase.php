<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\SystemConfigFormBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\SystemConfigFormBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'system_config_form' theme builder.
 */
abstract class SystemConfigFormBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'system_config_form'];

  /**
   * Set the method property on the system_config_form.
   */
  public function setMethod($value) {
    $this->renderable['#method'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the system_config_form.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

}
