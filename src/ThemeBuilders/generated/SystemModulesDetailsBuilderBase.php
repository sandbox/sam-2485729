<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\SystemModulesDetailsBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\SystemModulesDetailsBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'system_modules_details' theme builder.
 */
abstract class SystemModulesDetailsBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'system_modules_details'];

  /**
   * Set the method property on the system_modules_details.
   */
  public function setMethod($value) {
    $this->renderable['#method'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the system_modules_details.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

}
