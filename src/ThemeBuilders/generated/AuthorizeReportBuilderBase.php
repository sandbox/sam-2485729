<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\AuthorizeReportBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\AuthorizeReportBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'authorize_report' theme builder.
 */
abstract class AuthorizeReportBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'authorize_report'];

  /**
   * Set the messages property on the authorize_report.
   */
  public function setMessages($value) {
    $this->renderable['#messages'] = $value;
    return $this;
  }
}
