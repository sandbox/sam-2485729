<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ConfirmFormBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\ConfirmFormBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'confirm_form' theme builder.
 */
abstract class ConfirmFormBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'confirm_form'];

  /**
   * Set the method property on the confirm_form.
   */
  public function setMethod($value) {
    $this->renderable['#method'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the confirm_form.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

}
