<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\RdfMetadataBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\RdfMetadataBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'rdf_metadata' theme builder.
 */
abstract class RdfMetadataBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'rdf_metadata'];

  /**
   * Set the metadata property on the rdf_metadata.
   */
  public function setMetadata($value) {
    $this->renderable['#metadata'] = $value;
    return $this;
  }
}
