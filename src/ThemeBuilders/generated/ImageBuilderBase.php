<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ImageBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\ImageBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'image' theme builder.
 */
abstract class ImageBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'image'];

  /**
   * Set the uri property on the image.
   */
  public function setUri($value) {
    $this->renderable['#uri'] = $value;
    return $this;
  }
  /**
   * Set the width property on the image.
   */
  public function setWidth($value) {
    $this->renderable['#width'] = $value;
    return $this;
  }
  /**
   * Set the height property on the image.
   */
  public function setHeight($value) {
    $this->renderable['#height'] = $value;
    return $this;
  }
  /**
   * Set the alt property on the image.
   */
  public function setAlt($value) {
    $this->renderable['#alt'] = $value;
    return $this;
  }
  /**
   * Set the title property on the image.
   */
  public function setTitle($value) {
    $this->renderable['#title'] = $value;
    return $this;
  }
  /**
   * Set the attributes property on the image.
   */
  public function setAttributes($value) {
    $this->renderable['#attributes'] = $value;
    return $this;
  }
  /**
   * Set the sizes property on the image.
   */
  public function setSizes($value) {
    $this->renderable['#sizes'] = $value;
    return $this;
  }
  /**
   * Set the srcset property on the image.
   */
  public function setSrcset($value) {
    $this->renderable['#srcset'] = $value;
    return $this;
  }
  /**
   * Set the style_name property on the image.
   */
  public function setStyleName($value) {
    $this->renderable['#style_name'] = $value;
    return $this;
  }
}
