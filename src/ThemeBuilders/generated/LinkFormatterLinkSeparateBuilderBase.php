<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\LinkFormatterLinkSeparateBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\LinkFormatterLinkSeparateBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'link_formatter_link_separate' theme builder.
 */
abstract class LinkFormatterLinkSeparateBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'link_formatter_link_separate'];

  /**
   * Set the title property on the link_formatter_link_separate.
   */
  public function setTitle($value) {
    $this->renderable['#title'] = $value;
    return $this;
  }
  /**
   * Set the url_title property on the link_formatter_link_separate.
   */
  public function setUrlTitle($value) {
    $this->renderable['#url_title'] = $value;
    return $this;
  }
  /**
   * Set the url property on the link_formatter_link_separate.
   */
  public function setUrl($value) {
    $this->renderable['#url'] = $value;
    return $this;
  }
}
