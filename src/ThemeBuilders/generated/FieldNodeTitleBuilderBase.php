<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FieldNodeTitleBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\FieldNodeTitleBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'field__node__title' theme builder.
 */
abstract class FieldNodeTitleBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'field__node__title'];

}
