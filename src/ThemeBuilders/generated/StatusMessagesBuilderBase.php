<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\StatusMessagesBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\StatusMessagesBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'status_messages' theme builder.
 */
abstract class StatusMessagesBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'status_messages'];

  /**
   * Set the display property on the status_messages.
   */
  public function setDisplay($value) {
    $this->renderable['#display'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the status_messages.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

}
