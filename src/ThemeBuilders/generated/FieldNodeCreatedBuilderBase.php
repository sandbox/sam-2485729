<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FieldNodeCreatedBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\FieldNodeCreatedBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'field__node__created' theme builder.
 */
abstract class FieldNodeCreatedBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'field__node__created'];

}
