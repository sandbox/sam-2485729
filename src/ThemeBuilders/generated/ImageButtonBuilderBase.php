<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ImageButtonBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\ImageButtonBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'image_button' theme builder.
 */
abstract class ImageButtonBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'image_button'];

  /**
   * Set the return_value property on the image_button.
   */
  public function setReturnValue($value) {
    $this->renderable['#return_value'] = $value;
    return $this;
  }

  /**
   * Set the has_garbage_value property on the image_button.
   */
  public function setHasGarbageValue($value) {
    $this->renderable['#has_garbage_value'] = $value;
    return $this;
  }

  /**
   * Set the src property on the image_button.
   */
  public function setSrc($value) {
    $this->renderable['#src'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the image_button.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

  /**
   * Set the executes_submit_callback property on the image_button.
   */
  public function setExecutesSubmitCallback($value) {
    $this->renderable['#executes_submit_callback'] = $value;
    return $this;
  }

  /**
   * Set the input property on the image_button.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the name property on the image_button.
   */
  public function setName($value) {
    $this->renderable['#name'] = $value;
    return $this;
  }

  /**
   * Set the is_button property on the image_button.
   */
  public function setIsButton($value) {
    $this->renderable['#is_button'] = $value;
    return $this;
  }

  /**
   * Set the limit_validation_errors property on the image_button.
   */
  public function setLimitValidationErrors($value) {
    $this->renderable['#limit_validation_errors'] = $value;
    return $this;
  }

  /**
   * Set the process property on the image_button.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the image_button.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the image_button.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
