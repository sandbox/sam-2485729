<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\CkeditorSettingsToolbarBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\CkeditorSettingsToolbarBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'ckeditor_settings_toolbar' theme builder.
 */
abstract class CkeditorSettingsToolbarBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'ckeditor_settings_toolbar'];

  /**
   * Set the editor property on the ckeditor_settings_toolbar.
   */
  public function setEditor($value) {
    $this->renderable['#editor'] = $value;
    return $this;
  }
  /**
   * Set the plugins property on the ckeditor_settings_toolbar.
   */
  public function setPlugins($value) {
    $this->renderable['#plugins'] = $value;
    return $this;
  }
}
