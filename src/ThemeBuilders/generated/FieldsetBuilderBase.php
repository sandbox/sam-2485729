<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FieldsetBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\FieldsetBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'fieldset' theme builder.
 */
abstract class FieldsetBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'fieldset'];

  /**
   * Set the process property on the fieldset.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the fieldset.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the value property on the fieldset.
   */
  public function setValue($value) {
    $this->renderable['#value'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the fieldset.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

}
