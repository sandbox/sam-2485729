<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\DatetimeFormBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\DatetimeFormBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'datetime_form' theme builder.
 */
abstract class DatetimeFormBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'datetime_form'];

}
