<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ItemBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\ItemBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'item' theme builder.
 */
abstract class ItemBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'item'];

  /**
   * Set the input property on the item.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the markup property on the item.
   */
  public function setMarkup($value) {
    $this->renderable['#markup'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the item.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the item.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
