<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FeedIconBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\FeedIconBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'feed_icon' theme builder.
 */
abstract class FeedIconBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'feed_icon'];

  /**
   * Set the url property on the feed_icon.
   */
  public function setUrl($value) {
    $this->renderable['#url'] = $value;
    return $this;
  }
  /**
   * Set the title property on the feed_icon.
   */
  public function setTitle($value) {
    $this->renderable['#title'] = $value;
    return $this;
  }
}
