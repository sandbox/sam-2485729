<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\NodeEditFormBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\NodeEditFormBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'node_edit_form' theme builder.
 */
abstract class NodeEditFormBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'node_edit_form'];

  /**
   * Set the method property on the node_edit_form.
   */
  public function setMethod($value) {
    $this->renderable['#method'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the node_edit_form.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

}
