<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\MaintenancePageBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\MaintenancePageBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'maintenance_page' theme builder.
 */
abstract class MaintenancePageBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'maintenance_page'];

  /**
   * Set the title property on the maintenance_page.
   */
  public function setTitle($value) {
    $this->renderable['#title'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the maintenance_page.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

}
