<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\BlockSystemMenuBlockBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\BlockSystemMenuBlockBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'block__system_menu_block' theme builder.
 */
abstract class BlockSystemMenuBlockBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'block__system_menu_block'];

}
