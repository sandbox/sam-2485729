<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\SelectBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\SelectBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'select' theme builder.
 */
abstract class SelectBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'select'];

  /**
   * Set the input property on the select.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the multiple property on the select.
   */
  public function setMultiple($value) {
    $this->renderable['#multiple'] = $value;
    return $this;
  }

  /**
   * Set the process property on the select.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the select.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the select.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

  /**
   * Set the options property on the select.
   */
  public function setOptions($value) {
    $this->renderable['#options'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the select.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
