<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ImageScaleSummaryBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\ImageScaleSummaryBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'image_scale_summary' theme builder.
 */
abstract class ImageScaleSummaryBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'image_scale_summary'];

  /**
   * Set the data property on the image_scale_summary.
   */
  public function setData($value) {
    $this->renderable['#data'] = $value;
    return $this;
  }
  /**
   * Set the effect property on the image_scale_summary.
   */
  public function setEffect($value) {
    $this->renderable['#effect'] = $value;
    return $this;
  }
}
