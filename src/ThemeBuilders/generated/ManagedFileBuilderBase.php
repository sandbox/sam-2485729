<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ManagedFileBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\ManagedFileBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'managed_file' theme builder.
 */
abstract class ManagedFileBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'managed_file'];

  /**
   * Set the input property on the managed_file.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the process property on the managed_file.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the element_validate property on the managed_file.
   */
  public function setElementValidate($value) {
    $this->renderable['#element_validate'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the managed_file.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the managed_file.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

  /**
   * Set the progress_indicator property on the managed_file.
   */
  public function setProgressIndicator($value) {
    $this->renderable['#progress_indicator'] = $value;
    return $this;
  }

  /**
   * Set the progress_message property on the managed_file.
   */
  public function setProgressMessage($value) {
    $this->renderable['#progress_message'] = $value;
    return $this;
  }

  /**
   * Set the upload_validators property on the managed_file.
   */
  public function setUploadValidators($value) {
    $this->renderable['#upload_validators'] = $value;
    return $this;
  }

  /**
   * Set the upload_location property on the managed_file.
   */
  public function setUploadLocation($value) {
    $this->renderable['#upload_location'] = $value;
    return $this;
  }

  /**
   * Set the size property on the managed_file.
   */
  public function setSize($value) {
    $this->renderable['#size'] = $value;
    return $this;
  }

  /**
   * Set the multiple property on the managed_file.
   */
  public function setMultiple($value) {
    $this->renderable['#multiple'] = $value;
    return $this;
  }

  /**
   * Set the extended property on the managed_file.
   */
  public function setExtended($value) {
    $this->renderable['#extended'] = $value;
    return $this;
  }

  /**
   * Set the attached property on the managed_file.
   */
  public function setAttached($value) {
    $this->renderable['#attached'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the managed_file.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
