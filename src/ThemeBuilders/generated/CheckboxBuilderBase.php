<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\CheckboxBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\CheckboxBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'checkbox' theme builder.
 */
abstract class CheckboxBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'checkbox'];

  /**
   * Set the input property on the checkbox.
   */
  public function setInput($value) {
    $this->renderable['#input'] = $value;
    return $this;
  }

  /**
   * Set the return_value property on the checkbox.
   */
  public function setReturnValue($value) {
    $this->renderable['#return_value'] = $value;
    return $this;
  }

  /**
   * Set the process property on the checkbox.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the checkbox.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the checkbox.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

  /**
   * Set the title_display property on the checkbox.
   */
  public function setTitleDisplay($value) {
    $this->renderable['#title_display'] = $value;
    return $this;
  }

  /**
   * Set the value_callback property on the checkbox.
   */
  public function setValueCallback($value) {
    $this->renderable['#value_callback'] = $value;
    return $this;
  }

}
