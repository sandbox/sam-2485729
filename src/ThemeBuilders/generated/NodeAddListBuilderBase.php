<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\NodeAddListBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\NodeAddListBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'node_add_list' theme builder.
 */
abstract class NodeAddListBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'node_add_list'];

  /**
   * Set the content property on the node_add_list.
   */
  public function setContent($value) {
    $this->renderable['#content'] = $value;
    return $this;
  }
}
