<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ItemListBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\ItemListBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'item_list' theme builder.
 */
abstract class ItemListBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'item_list'];

  /**
   * Set the items property on the item_list.
   */
  public function setItems($value) {
    $this->renderable['#items'] = $value;
    return $this;
  }
  /**
   * Set the title property on the item_list.
   */
  public function setTitle($value) {
    $this->renderable['#title'] = $value;
    return $this;
  }
  /**
   * Set the list_type property on the item_list.
   */
  public function setListType($value) {
    $this->renderable['#list_type'] = $value;
    return $this;
  }
  /**
   * Set the attributes property on the item_list.
   */
  public function setAttributes($value) {
    $this->renderable['#attributes'] = $value;
    return $this;
  }
  /**
   * Set the empty property on the item_list.
   */
  public function setEmpty($value) {
    $this->renderable['#empty'] = $value;
    return $this;
  }
}
