<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\MenuLocalTaskBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\MenuLocalTaskBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'menu_local_task' theme builder.
 */
abstract class MenuLocalTaskBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'menu_local_task'];

}
