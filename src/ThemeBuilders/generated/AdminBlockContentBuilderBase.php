<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\AdminBlockContentBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\AdminBlockContentBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'admin_block_content' theme builder.
 */
abstract class AdminBlockContentBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'admin_block_content'];

  /**
   * Set the content property on the admin_block_content.
   */
  public function setContent($value) {
    $this->renderable['#content'] = $value;
    return $this;
  }
}
