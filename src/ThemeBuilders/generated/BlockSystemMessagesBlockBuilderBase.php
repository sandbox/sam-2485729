<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\BlockSystemMessagesBlockBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\BlockSystemMessagesBlockBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'block__system_messages_block' theme builder.
 */
abstract class BlockSystemMessagesBlockBuilderBase extends BuilderBase {


}
