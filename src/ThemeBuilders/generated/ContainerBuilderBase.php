<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ContainerBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\ContainerBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'container' theme builder.
 */
abstract class ContainerBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'container'];

  /**
   * Set the process property on the container.
   */
  public function setProcess($value) {
    $this->renderable['#process'] = $value;
    return $this;
  }

  /**
   * Set the pre_render property on the container.
   */
  public function setPreRender($value) {
    $this->renderable['#pre_render'] = $value;
    return $this;
  }

  /**
   * Set the theme_wrappers property on the container.
   */
  public function setThemeWrappers($value) {
    $this->renderable['#theme_wrappers'] = $value;
    return $this;
  }

}
