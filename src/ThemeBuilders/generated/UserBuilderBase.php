<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\UserBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\UserBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'user' theme builder.
 */
abstract class UserBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'user'];

}
