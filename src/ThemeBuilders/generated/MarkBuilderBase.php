<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\MarkBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\MarkBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'mark' theme builder.
 */
abstract class MarkBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'mark'];

  /**
   * Set the status property on the mark.
   */
  public function setStatus($value) {
    $this->renderable['#status'] = $value;
    return $this;
  }
}
