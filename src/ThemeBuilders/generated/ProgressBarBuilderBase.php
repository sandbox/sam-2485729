<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ProgressBarBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\ProgressBarBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'progress_bar' theme builder.
 */
abstract class ProgressBarBuilderBase extends BuilderBase {

  protected $renderable = ['#theme' => 'progress_bar'];

  /**
   * Set the label property on the progress_bar.
   */
  public function setLabel($value) {
    $this->renderable['#label'] = $value;
    return $this;
  }
  /**
   * Set the percent property on the progress_bar.
   */
  public function setPercent($value) {
    $this->renderable['#percent'] = $value;
    return $this;
  }
  /**
   * Set the message property on the progress_bar.
   */
  public function setMessage($value) {
    $this->renderable['#message'] = $value;
    return $this;
  }
}
