<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\RegionBuilderBase.
 *
 * WARNING: This is a generated class, do not modify. Instead modify:
 * \Drupal\theme_builder\ThemeBuilders\generated\RegionBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders\generated;

use Drupal\theme_builder\BuilderBase;

/**
 * Base class for the 'region' theme builder.
 */
abstract class RegionBuilderBase extends BuilderBase {

  protected $renderable = ['#type' => 'region'];

}
