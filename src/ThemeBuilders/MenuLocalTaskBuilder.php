<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\MenuLocalTaskBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\MenuLocalTaskBuilderBase;

/**
 * The menu_local_task theme builder.
 */
class MenuLocalTaskBuilder extends MenuLocalTaskBuilderBase {
}
