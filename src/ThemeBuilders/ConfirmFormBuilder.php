<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ConfirmFormBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\ConfirmFormBuilderBase;

/**
 * The confirm_form theme builder.
 */
class ConfirmFormBuilder extends ConfirmFormBuilderBase {
}
