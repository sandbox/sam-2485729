<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ImageBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\ImageBuilderBase;

/**
 * The image theme builder.
 */
class ImageBuilder extends ImageBuilderBase {
}
