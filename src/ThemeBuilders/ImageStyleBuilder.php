<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ImageStyleBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\ImageStyleBuilderBase;

/**
 * The image_style theme builder.
 */
class ImageStyleBuilder extends ImageStyleBuilderBase {
}
