<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\CheckboxesBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\BuilderHelpers\FormElementTrait;
use Drupal\theme_builder\ThemeBuilders\generated\CheckboxesBuilderBase;

/**
 * The checkboxes theme builder.
 */
class CheckboxesBuilder extends CheckboxesBuilderBase {

  use FormElementTrait;

}
