<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ImageWidgetBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\ImageWidgetBuilderBase;

/**
 * The image_widget theme builder.
 */
class ImageWidgetBuilder extends ImageWidgetBuilderBase {
}
