<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\EmailBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\EmailBuilderBase;

/**
 * The email theme builder.
 */
class EmailBuilder extends EmailBuilderBase {
}
