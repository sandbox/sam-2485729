<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\DatetimeFormBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\DatetimeFormBuilderBase;

/**
 * The datetime_form theme builder.
 */
class DatetimeFormBuilder extends DatetimeFormBuilderBase {
}
