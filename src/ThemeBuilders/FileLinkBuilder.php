<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FileLinkBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\FileLinkBuilderBase;

/**
 * The file_link theme builder.
 */
class FileLinkBuilder extends FileLinkBuilderBase {
}
