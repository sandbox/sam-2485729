<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\MaintenancePageBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\MaintenancePageBuilderBase;

/**
 * The maintenance_page theme builder.
 */
class MaintenancePageBuilder extends MaintenancePageBuilderBase {
}
