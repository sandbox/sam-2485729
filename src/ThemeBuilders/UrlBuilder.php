<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\UrlBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\UrlBuilderBase;

/**
 * The url theme builder.
 */
class UrlBuilder extends UrlBuilderBase {
}
