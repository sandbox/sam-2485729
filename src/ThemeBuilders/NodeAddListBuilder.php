<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\NodeAddListBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\NodeAddListBuilderBase;

/**
 * The node_add_list theme builder.
 */
class NodeAddListBuilder extends NodeAddListBuilderBase {
}
