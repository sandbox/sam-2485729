<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\SystemModulesUninstallBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\SystemModulesUninstallBuilderBase;

/**
 * The system_modules_uninstall theme builder.
 */
class SystemModulesUninstallBuilder extends SystemModulesUninstallBuilderBase {
}
