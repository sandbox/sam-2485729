<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\MenuLocalActionBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\MenuLocalActionBuilderBase;

/**
 * The menu_local_action theme builder.
 */
class MenuLocalActionBuilder extends MenuLocalActionBuilderBase {
}
