<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\RdfMetadataBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\RdfMetadataBuilderBase;

/**
 * The rdf_metadata theme builder.
 */
class RdfMetadataBuilder extends RdfMetadataBuilderBase {
}
