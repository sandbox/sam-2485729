<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\SystemThemesPageBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\SystemThemesPageBuilderBase;

/**
 * The system_themes_page theme builder.
 */
class SystemThemesPageBuilder extends SystemThemesPageBuilderBase {
}
