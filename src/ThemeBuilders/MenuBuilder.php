<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\MenuBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\MenuBuilderBase;

/**
 * The menu theme builder.
 */
class MenuBuilder extends MenuBuilderBase {
}
