<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ImageFormatterBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\ImageFormatterBuilderBase;

/**
 * The image_formatter theme builder.
 */
class ImageFormatterBuilder extends ImageFormatterBuilderBase {
}
