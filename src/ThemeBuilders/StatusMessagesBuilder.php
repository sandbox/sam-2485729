<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\StatusMessagesBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\StatusMessagesBuilderBase;

/**
 * The status_messages theme builder.
 */
class StatusMessagesBuilder extends StatusMessagesBuilderBase {
}
