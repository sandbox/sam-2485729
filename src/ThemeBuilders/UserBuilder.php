<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\UserBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\UserBuilderBase;

/**
 * The user theme builder.
 */
class UserBuilder extends UserBuilderBase {
}
