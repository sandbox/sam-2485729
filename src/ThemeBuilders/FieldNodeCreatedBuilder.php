<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FieldNodeCreatedBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\FieldNodeCreatedBuilderBase;

/**
 * The field__node__created theme builder.
 */
class FieldNodeCreatedBuilder extends FieldNodeCreatedBuilderBase {
}
