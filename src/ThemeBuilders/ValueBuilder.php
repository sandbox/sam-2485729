<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ValueBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\ValueBuilderBase;

/**
 * The value theme builder.
 */
class ValueBuilder extends ValueBuilderBase {
}
