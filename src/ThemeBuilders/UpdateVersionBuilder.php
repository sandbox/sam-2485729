<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\UpdateVersionBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\UpdateVersionBuilderBase;

/**
 * The update_version theme builder.
 */
class UpdateVersionBuilder extends UpdateVersionBuilderBase {
}
