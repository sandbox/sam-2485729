<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\UpdateLastCheckBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\UpdateLastCheckBuilderBase;

/**
 * The update_last_check theme builder.
 */
class UpdateLastCheckBuilder extends UpdateLastCheckBuilderBase {
}
