<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\IndentationBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\IndentationBuilderBase;

/**
 * The indentation theme builder.
 */
class IndentationBuilder extends IndentationBuilderBase {
}
