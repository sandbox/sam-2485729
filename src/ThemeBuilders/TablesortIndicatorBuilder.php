<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\TablesortIndicatorBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\TablesortIndicatorBuilderBase;

/**
 * The tablesort_indicator theme builder.
 */
class TablesortIndicatorBuilder extends TablesortIndicatorBuilderBase {
}
