<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\InstallPageBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\InstallPageBuilderBase;

/**
 * The install_page theme builder.
 */
class InstallPageBuilder extends InstallPageBuilderBase {
}
