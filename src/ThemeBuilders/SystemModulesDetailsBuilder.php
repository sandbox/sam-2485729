<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\SystemModulesDetailsBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\SystemModulesDetailsBuilderBase;

/**
 * The system_modules_details theme builder.
 */
class SystemModulesDetailsBuilder extends SystemModulesDetailsBuilderBase {
}
