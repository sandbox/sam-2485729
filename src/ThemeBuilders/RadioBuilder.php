<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\RadioBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\BuilderHelpers\FormElementTrait;
use Drupal\theme_builder\ThemeBuilders\generated\RadioBuilderBase;

/**
 * The radio theme builder.
 */
class RadioBuilder extends RadioBuilderBase {

  use FormElementTrait;

}
