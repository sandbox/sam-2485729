<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ButtonBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\ButtonBuilderBase;

/**
 * The button theme builder.
 */
class ButtonBuilder extends ButtonBuilderBase {
}
