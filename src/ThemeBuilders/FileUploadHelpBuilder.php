<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FileUploadHelpBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\FileUploadHelpBuilderBase;

/**
 * The file_upload_help theme builder.
 */
class FileUploadHelpBuilder extends FileUploadHelpBuilderBase {
}
