<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\NodeEditFormBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\NodeEditFormBuilderBase;

/**
 * The node_edit_form theme builder.
 */
class NodeEditFormBuilder extends NodeEditFormBuilderBase {
}
