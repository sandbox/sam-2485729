<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FormBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\BuilderHelpers\ValidationTrait;
use Drupal\theme_builder\ThemeBuilders\generated\FormBuilderBase;

/**
 * The form theme builder.
 */
class FormBuilder extends FormBuilderBase {

  use ValidationTrait;

  /**
   * Set the children of this element.
   *
   * @param array $children
   *   The child elements of the element.
   *
   * @return $this
   */
  public function setChildren($children) {
    $this->renderable['#children'] = $this->validateArray($children);
    return $this;
  }

}
