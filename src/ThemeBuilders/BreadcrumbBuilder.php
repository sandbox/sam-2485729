<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\BreadcrumbBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\BreadcrumbBuilderBase;

/**
 * The breadcrumb theme builder.
 */
class BreadcrumbBuilder extends BreadcrumbBuilderBase {
}
