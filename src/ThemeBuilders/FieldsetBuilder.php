<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FieldsetBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\BuilderHelpers\FormElementTrait;
use Drupal\theme_builder\ThemeBuilders\generated\FieldsetBuilderBase;

/**
 * The fieldset theme builder.
 */
class FieldsetBuilder extends FieldsetBuilderBase {

  use FormElementTrait;

}
