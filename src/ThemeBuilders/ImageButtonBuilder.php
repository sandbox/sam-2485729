<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ImageButtonBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\ImageButtonBuilderBase;

/**
 * The image_button theme builder.
 */
class ImageButtonBuilder extends ImageButtonBuilderBase {
}
