<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\HtmlBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\HtmlBuilderBase;

/**
 * The html theme builder.
 */
class HtmlBuilder extends HtmlBuilderBase {
}
