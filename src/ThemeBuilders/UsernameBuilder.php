<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\UsernameBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\UsernameBuilderBase;

/**
 * The username theme builder.
 */
class UsernameBuilder extends UsernameBuilderBase {
}
