<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\SubmitBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\SubmitBuilderBase;

/**
 * The submit theme builder.
 */
class SubmitBuilder extends SubmitBuilderBase {
}
