<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\MaintenanceTaskListBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\MaintenanceTaskListBuilderBase;

/**
 * The maintenance_task_list theme builder.
 */
class MaintenanceTaskListBuilder extends MaintenanceTaskListBuilderBase {
}
