<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\CkeditorSettingsToolbarBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\CkeditorSettingsToolbarBuilderBase;

/**
 * The ckeditor_settings_toolbar theme builder.
 */
class CkeditorSettingsToolbarBuilder extends CkeditorSettingsToolbarBuilderBase {
}
