<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\CommentBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\CommentBuilderBase;

/**
 * The comment theme builder.
 */
class CommentBuilder extends CommentBuilderBase {
}
