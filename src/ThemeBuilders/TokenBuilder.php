<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\TokenBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\TokenBuilderBase;

/**
 * The token theme builder.
 */
class TokenBuilder extends TokenBuilderBase {
}
