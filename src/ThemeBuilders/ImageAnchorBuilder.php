<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ImageAnchorBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\ImageAnchorBuilderBase;

/**
 * The image_anchor theme builder.
 */
class ImageAnchorBuilder extends ImageAnchorBuilderBase {
}
