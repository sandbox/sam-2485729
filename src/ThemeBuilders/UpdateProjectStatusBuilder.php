<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\UpdateProjectStatusBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\UpdateProjectStatusBuilderBase;

/**
 * The update_project_status theme builder.
 */
class UpdateProjectStatusBuilder extends UpdateProjectStatusBuilderBase {
}
