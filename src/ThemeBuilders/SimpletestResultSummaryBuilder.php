<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\SimpletestResultSummaryBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\SimpletestResultSummaryBuilderBase;

/**
 * The simpletest_result_summary theme builder.
 */
class SimpletestResultSummaryBuilder extends SimpletestResultSummaryBuilderBase {
}
