<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\BlockListBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\BlockListBuilderBase;

/**
 * The block_list theme builder.
 */
class BlockListBuilder extends BlockListBuilderBase {
}
