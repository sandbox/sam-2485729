<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FeedIconBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\FeedIconBuilderBase;

/**
 * The feed_icon theme builder.
 */
class FeedIconBuilder extends FeedIconBuilderBase {
}
