<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\BlockSystemMenuBlockBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\BlockSystemMenuBlockBuilderBase;

/**
 * The block__system_menu_block theme builder.
 */
class BlockSystemMenuBlockBuilder extends BlockSystemMenuBlockBuilderBase {
}
