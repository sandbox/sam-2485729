<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\MenuLocalTasksBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\MenuLocalTasksBuilderBase;

/**
 * The menu_local_tasks theme builder.
 */
class MenuLocalTasksBuilder extends MenuLocalTasksBuilderBase {
}
