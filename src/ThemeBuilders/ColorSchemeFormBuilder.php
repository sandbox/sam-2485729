<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ColorSchemeFormBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\ColorSchemeFormBuilderBase;

/**
 * The color_scheme_form theme builder.
 */
class ColorSchemeFormBuilder extends ColorSchemeFormBuilderBase {
}
