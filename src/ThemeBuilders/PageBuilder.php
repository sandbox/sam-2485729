<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\PageBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\PageBuilderBase;

/**
 * The page theme builder.
 */
class PageBuilder extends PageBuilderBase {
}
