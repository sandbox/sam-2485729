<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\SearchResultBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\SearchResultBuilderBase;

/**
 * The search_result theme builder.
 */
class SearchResultBuilder extends SearchResultBuilderBase {
}
