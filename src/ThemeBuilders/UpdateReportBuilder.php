<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\UpdateReportBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\UpdateReportBuilderBase;

/**
 * The update_report theme builder.
 */
class UpdateReportBuilder extends UpdateReportBuilderBase {
}
