<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\ItemBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\ItemBuilderBase;

/**
 * The item theme builder.
 */
class ItemBuilder extends ItemBuilderBase {
}
