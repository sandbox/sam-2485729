<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\DatelistBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\DatelistBuilderBase;

/**
 * The datelist theme builder.
 */
class DatelistBuilder extends DatelistBuilderBase {
}
