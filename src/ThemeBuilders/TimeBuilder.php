<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\TimeBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\TimeBuilderBase;

/**
 * The time theme builder.
 */
class TimeBuilder extends TimeBuilderBase {

}
