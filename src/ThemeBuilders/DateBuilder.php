<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\DateBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\DateBuilderBase;

/**
 * The date theme builder.
 */
class DateBuilder extends DateBuilderBase {
}
