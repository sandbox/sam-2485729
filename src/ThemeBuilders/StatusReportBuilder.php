<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\StatusReportBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\StatusReportBuilderBase;

/**
 * The status_report theme builder.
 */
class StatusReportBuilder extends StatusReportBuilderBase {
}
