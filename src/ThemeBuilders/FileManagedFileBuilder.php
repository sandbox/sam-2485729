<?php

/**
 * @file
 * Contains Drupal\theme_builder\ThemeBuilders\FileManagedFileBuilder.
 */

namespace Drupal\theme_builder\ThemeBuilders;

use Drupal\theme_builder\ThemeBuilders\generated\FileManagedFileBuilderBase;

/**
 * The file_managed_file theme builder.
 */
class FileManagedFileBuilder extends FileManagedFileBuilderBase {
}
