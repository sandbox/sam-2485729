<?php

/**
 * @file
 * Contains Drupal\theme_builder\BuilderHelpers\FormElementTrait.
 */

namespace Drupal\theme_builder\BuilderHelpers;
use Drupal\theme_builder\ThemeBuilders\InputBuilder;
use Drupal\theme_builder\ThemeBuilders\TableBuilder;

/**
 * Provide a trait to set the common form element properties.
 *
 * Include setters that are found on most (but not-necessarily all) form
 * elements.
 */
trait FormElementTrait {

  use ValidationTrait;

  /**
   * Set the title field.
   *
   * @param mixed $value
   *   The value to set
   *
   * @return $this
   *   Instance of target class.
   */
  public function setTitle($value) {
    $this->set('title', $value);
    return $this;
  }

  /**
   * Set the description field.
   *
   * @param mixed $value
   *   The value to set
   *
   * @return $this
   *   Instance of target class.
   */
  public function setDescription($value) {
    $this->set('description', $value);
    return $this;
  }

  /**
   * Set the element_validate field.
   *
   * @param mixed $value
   *   The value to set
   *
   * @return $this
   *   Instance of target class.
   */
  public function setElementValidate($value) {
    $this->set('element_validate', $this->validateArray($value));
    return $this;
  }

  /**
   * Set the disabled field.
   *
   * @param mixed $value
   *   The value to set
   *
   * @return $this
   *   Instance of target class.s
   */
  public function setDisabled($value) {
    $this->set('disabled', $value);
    return $this;
  }

  /**
   * Set the value field.
   *
   * @param mixed $value
   *   The value to set
   *
   * @return $this
   *   Instance of target class.
   */
  public function setValue($value) {
    $this->set('value', $value);
    return $this;
  }

  /**
   * Set the default value field.
   *
   * @param mixed $value
   *   The value to set
   *
   * @return $this
   *   Instance of target class.
   */
  public function setDefaultValue($value) {
    $this->set('default_value', $value);
    return $this;
  }

  /**
   * Set the default value field.
   *
   * @param mixed $value
   *   The value to set
   *
   * @return $this
   *   Instance of target class.
   */
  public function setPrefix($value) {
    $this->set('prefix', $value);
    return $this;
  }

  /**
   * Set the required field.
   *
   * @param mixed $value
   *   The value to set
   *
   * @return $this
   *   Instance of target class.
   */
  public function setRequired($value) {
    $this->set('required', $this->validateBoolean($value));
    return $this;
  }

  /**
   * Set the suffix field.
   *
   * @param mixed $value
   *   The value to set
   *
   * @return $this
   *   Instance of target class.
   */
  public function setSuffix($value) {
    $this->set('suffix', $value);
    return $this;
  }

  /**
   * Set the states field.
   *
   * @param mixed $value
   *   The value to set
   *
   * @return $this
   *   Instance of target class.
   */
  public function setStates($value) {
    $this->set('states', $value);
    return $this;
  }

  /**
   * Set the title display field.
   *
   * @param mixed $value
   *   The value to set
   *
   * @return $this
   *   Instance of target class.
   */
  public function setTitleDisplay($value) {
    $this->set('title_display', $value);
    return $this;
  }

  /**
   * Set the tree field.
   *
   * @param mixed $value
   *   The value to set
   *
   * @return $this
   *   Instance of target class.
   */
  public function setTree($value) {
    $this->set('tree', $value);
    return $this;
  }

  /**
   * Set the value callback field.
   *
   * @param mixed $value
   *   The value to set
   *
   * @return $this
   *   Instance of target class.
   */
  public function setValueCallback($value) {
    $this->set('value_callback', $value);
    return $this;
  }

  /**
   * Set the weight field.
   *
   * @param mixed $value
   *   The value to set
   *
   * @return $this
   *   Instance of target class.
   */
  public function setWeight($value) {
    $this->set('tree', $value);
    return $this;
  }

}
