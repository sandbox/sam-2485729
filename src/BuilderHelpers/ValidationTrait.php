<?php

/**
 * @file
 * Contains Drupal\theme_builder\BuilderHelpers\ValidationTrait.
 */

namespace Drupal\theme_builder\BuilderHelpers;

/**
 * Simple validation useful for checking types of incoming variables.
 */
trait ValidationTrait {
  /**
   * Validate a string.
   *
   * @param mixed $value
   *   The value to validate.
   *
   * @throws \Exception
   *
   * @return mixed
   *   The validated value.
   */
  protected function validateString($value) {
    if (FALSE === is_string($value)) {
      throw new \Exception('Could not validate builder argument as string.');
    }
    return $value;
  }

  /**
   * Validate a numeric value.
   *
   * @param mixed $value
   *   The value to validate.
   *
   * @throws \Exception
   *
   * @return mixed
   *   The validated value.
   */
  protected function validateNumeric($value) {
    if (FALSE === is_numeric($value)) {
      throw new \Exception('Could not validate builder argument as numeric.');
    }
    return $value;
  }

  /**
   * Validate an array.
   *
   * @param mixed $value
   *   The value to validate.
   *
   * @throws \Exception
   *
   * @return mixed
   *   The validated value.
   */
  protected function validateArray($value) {
    if (FALSE === is_array($value)) {
      throw new \Exception('Could not validate builder argument as array.');
    }
    return $value;
  }

  /**
   * Validate an array.
   *
   * @param mixed $value
   *   The value to validate.
   *
   * @throws \Exception
   *
   * @return mixed
   *   The validated value.
   */
  protected function validateBoolean($value) {
    if (FALSE === is_bool($value)) {
      throw new \Exception('Could not validate builder argument as boolean.');
    }
    return $value;
  }
}
