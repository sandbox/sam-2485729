<?php

/**
 * @file
 * Contains Drupal\theme_builder\ClassToFilesystemInterface.
 */

namespace Drupal\theme_builder;

/**
 * An interface that defines how classes are saved to the filesystem.
 */
interface ClassToFilesystemInterface {
  /**
   * Save the classes for the given module.
   *
   * @param $module
   * @return mixed
   */
  public function saveClasses($module);
}
