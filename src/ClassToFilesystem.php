<?php

/**
 * @file
 * Contains Drupal\theme_builder\ClassToFilesystem.
 */

namespace Drupal\theme_builder;

use Doctrine\Common\Inflector\Inflector;
use Drupal\Core\Extension\InfoParserInterface;

/**
 * Save classes to the filesystem.
 */
class ClassToFilesystem implements ClassToFilesystemInterface {

  /**
   * @var \Drupal\Core\Extension\InfoParserInterface
   */
  protected $infoParser;

  /**
   * @var \Drupal\Core\Extension\InfoParserInterface
   */
  protected $classGenerator;

  public function __construct(InfoParserInterface $info_parser, ClassGeneratorInterface $class_generator) {
    $this->infoParser = $info_parser;
    $this->classGenerator = $class_generator;
  }

  /**
   * {@inheritdoc}
   */
  public function saveClasses($module) {
    $theme_functions = $this->getThemeFunctions($module);
    foreach ($theme_functions as $theme_name) {
      // Never overwrite the stub since it's designed to be modified.
      $stub_location = $this->getClassLocation($theme_name, $module);
      if (FALSE === $this->fileExists($stub_location)) {
        $stub = $this->classGenerator->generateStub($theme_name, $module);
        $this->write($stub_location, $stub);
      }
      // Always overwrite the base class because it is generated.
      $base_location = $this->getClassLocation($theme_name, $module, TRUE);
      $base = $this->classGenerator->generateBase($theme_name, $module);
      $this->write($base_location, $base);
    }
  }

  /**
   * Get the location for where a class should live.
   *
   * @param $theme
   *   The theme name.
   * @param $module
   *   The module responsible for the theme.
   * @param bool $base
   *   If the class is the base class (or alternatively the stub).
   *
   * @return string
   *   The filesystem location.
   */
  protected function getClassLocation($theme, $module, $base = FALSE) {
    $filename = Inflector::classify($theme . ($base ? 'BuilderBase' : 'Builder')) . '.php';
    return DRUPAL_ROOT . '/' . $this->modulePath($module) . '/src/ThemeBuilders/' . ($base ? 'generated/' : '') . $filename;
  }

  /**
   * Get the theme functions associated with a module.
   *
   * @param $module
   *   The module name.
   * @return array
   *   An array of defined theme functions.
   */
  protected function getThemeFunctions($module) {
    $info_file = $this->modulePath($module) . '/' . $module . '.info.yml';
    $info = $this->infoParser->parse($info_file);
    return isset($info['theme_builders']) ? $info['theme_builders'] : [];
  }

  /**
   * Get the path to a module.
   *
   * @param string $module_name
   *   A module name.
   * @return string
   *   The path to a module relative to the drupal root.
   */
  protected function modulePath($module_name) {
    return drupal_get_path('module', $module_name);
  }

  /**
   * Check a file exists.
   *
   * @param $file
   *   Filename.
   *
   * @return bool
   *   Existence status.
   */
  protected function fileExists($file) {
    return file_exists($file);
  }

  /**
   * Write to the filesystem.
   *
   * @param $location
   *   The location on disk.
   * @param $renderable
   *   A drupal renderable array.
   *
   * @return int
   *   Success status code.
   */
  protected function write($location, $renderable) {
    return file_put_contents($location, render($renderable));
  }
}
