<?php

/**
 * @file
 * Contains Drupal\theme_builder\BuilderBaseInterface.
 */

namespace Drupal\theme_builder;

/**
 * The builder base interface.
 */
interface BuilderBaseInterface {
  /**
   * @return array
   *   The renderable array that has been built.
   */
  public function render();

  /**
   * Set the renderable array with some conent.
   *
   * @param array $content
   *   An array of content.
   */
  public function setRenderable($content);

  /**
   * @return string
   *   Get the rendered HTML.
   */
  public function getRenderedHtml();

  /**
   * Create a builder instance.
   */
  public static function create();

}
