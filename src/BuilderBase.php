<?php

/**
 * @file
 * Contains Drupal\theme_builder\BuilderBase.
 */

namespace Drupal\theme_builder;

use Drupal\theme_builder\BuilderHelpers\UniversalTrait;
use Drupal\theme_builder\BuilderHelpers\ValidationTrait;

/**
 * Define a base that all theme builders extend.
 */
class BuilderBase implements BuilderBaseInterface {

  protected $renderable = array();

  /**
   * {@inheritdoc}
   */
  public static function create() {
    return new static();
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return $this->renderable;
  }

  /**
   * {@inheritdoc}
   */
  public function setRenderable($renderable) {
    $this->renderable = $renderable;
  }

  /**
   * {@inheritdoc}
   */
  public function getRenderedHtml() {
    return $this->drupalRender($this->renderable);
  }

  /**
   * Set a generic key.
   *
   * @param $key
   *   Key to set.
   * @param $value
   *   The value.
   */
  protected function set($key, $value) {
    $this->renderable['#' . $key] = $value;
  }

  /**
   * Wrapper around drupal rendering.
   *
   * @param $content
   *   A drupal content array.
   * @return string
   *   Rendered HTML.
   */
  protected function drupalRender($content) {
    return render($content);
  }

  /**
   * Set the attributes field.
   *
   * This is a very common key which seems to be widely supported but difficult
   * to derive. Support it for all builders while running the risk that it is
   * not appropriate for some. Could be moved into the relevant builders if it
   * was creating confusion of false positives.
   *
   * @param mixed $value
   *   The value to set
   *
   * @return $this
   *   Instance of target class.
   */
  public function setAttributes($value) {
    $this->set('attributes', $this->validateArray($value));
    return $this;
  }

}
