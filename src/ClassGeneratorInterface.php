<?php

/**
 * @file
 * Contains Drupal\theme_builder\ClassGeneratorInterface.
 */

namespace Drupal\theme_builder;

/**
 * The class generator interface.
 */
interface ClassGeneratorInterface {
  /**
   * Generate base classes to automate the creation of common builder methods.
   *
   * @param $builder_key
   *   The theme definition to build a class for.
   * @param $module
   *   The target module the class will eventually be stored in.
   *
   * @return string
   *   A render array representing the class.
   */
  public function generateBase($builder_key, $module);

  /**
   * A stub is designed to be customised and extend the builder base.
   *
   * @param $builder_key
   *   The theme definition to build a class for.
   * @param $module
   *   The target module the class will eventually be stored in.
   *
   * @return string
   *   A render array representing the class.
   */
  public function generateStub($builder_key, $module);
}
