Theme Builder
=============

Provides a way to generate and create builder classes for theme defintions
defined in hook_theme(). This is a [dx improvement proposed by larowlan](https://www.drupal.org/node/2316941).

For bugs and feedback: https://www.drupal.org/sandbox/sam/2485729

Setup
=====

A module can elect to become responsibe for the builder classes of certain
theme functions. This module has chosen to provide builder classes for all of
the core theme definitions. In order to become responsible for a theme def,
add the following to a module's info file:

````
theme_builders:
 - theme_def_name
````

Once complete, ensure the src/ThemeBuilders and src/ThemeBuilders/generated
folders exist and run "drush theme-builder MODULE_NAME".

Enhancing Generated Builders
============================

You can enhance generated builders by providing setters for keys which weren't
able to be inferred and providing validation for existing setters. The classes
which were directly placed into src/ThemeBuilders will not be overriden by
drush. You can use these stubs to start writing code. These are the classes
which will be called.

Protected validators are provided for common validations:

````
class ItemListBuilder extends ItemListBuilderBase {
  public function setItems($value) {
    return parent::setItems($this->validateArray($value));
  }
}
````

Using the Builders
==================

To start using the builders, in your IDE, start typing a camelized version
of your theme key and append "Builder". The IDE should autocomplete the class
and you can start to chain expressions such as:

````
  ListItemBuilder::create()->setItems(['a', 'b'])->getRenderable();
````

Before:

````
  $something = [
    '#type' => 'container',
    '#attributes' => [
      'class' => 'test-class',
    ],
    '#children' => [
      [
        [
          '#theme' => 'image',
          'uri' => 'public://image.png',
        ],
        [
          '#theme' => 'table',
          '#header' => ['a', 'b'],
          '#rows' => [['c', 'd']],
        ]
      ]
    ],
  ];
````

After:

````
  $something = ContainerBuilder::create()
    ->setAttributes(['class' => 'test-class'])
    ->setChildren([
        ImageBuilder::create()
          ->setUri('public://image.png')
          ->getRenderable(),
        TableBuilder::create()
          ->setHeader(['a', 'b'])
          ->setRows([['c', 'd']])
          ->getRenderable()
      ]);
````

An example of IDE autocompletion:

![IDE Integration](https://www.drupal.org/files/Screen%20Shot%202015-05-09%20at%202.50.09%20am.png)
