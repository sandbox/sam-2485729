<?php

/**
 * @file
 * Drush command for the class generation.
 */

/**
 * Implements hook_drush_command().
 */
function theme_builder_drush_command() {
  return [
    'theme-builder' => [
      'description' => 'Use this commend with your module name as an argument to generate builder classes.',
      'arguments' => [
        'module' => 'Module machine name.',
      ],
      'options' => [
      ],
      'examples' => [
        'drush theme-builder MODULE_NAME' => 'Generate builder classes for the given module.',
      ],
    ]
  ];
}

/**
 * Drush command callback.
 */
function drush_theme_builder($module = NULL) {
  \Drupal::service('theme_builder.class_to_filesystem')->saveClasses($module);
}
