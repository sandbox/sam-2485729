<?php

/**
 * @file
 * Contains Drupal\Tests\UnitTestCase\ClassGeneratorTest.
 */

namespace Drupal\Tests\theme_builder\Unit;

use Doctrine\Common\Inflector\Inflector;
use Drupal\Tests\UnitTestCase;
use Drupal\theme_builder\ClassGenerator;

/**
 * Test the class generator.
 *
 * @group theme_builder
 */
class ClassGeneratorTest extends UnitTestCase {

  /**
   * @dataProvider classGeneratorDataProvider
   *
   * Test the class is generatored correctly.
   */
  public function testClassGenerator($builder_key, $theme_definition, $variable_methods, $render_element_methods) {

    $generator = new ClassGenerator(
      $this->getMockRenderElementManager(),
      $this->getMockThemeRegistry($theme_definition)
    );

    $stub_and_base = [
      $generator->generateBase($builder_key, 'test_module'),
      $generator->generateStub($builder_key, 'test_module'),
    ];

    // Test the base and stub classes.
    foreach ($stub_and_base as $is_stub => $generated_class) {
      if (isset($theme_definition[$builder_key]['variables'])) {
        $this->assertArrayEquals($generated_class['#methods'], $variable_methods);
        $this->assertFalse($render_element_methods);
      }
      if (isset($theme_definition[$builder_key]['render element'])) {
        $this->assertArrayEquals($generated_class['#render_element_methods'], $render_element_methods);
        $this->assertFalse($variable_methods);
      }
      $this->assertEquals($generated_class['#module'], 'test_module');
      $this->assertEquals($generated_class['#class_name'], Inflector::classify($builder_key . '_Builder'));
      $this->assertEquals($generated_class['#theme_name'], $builder_key);
      $this->assertEquals($generated_class['#theme'], $is_stub ? 'builder_class_stub' : 'builder_class_base');
    }
  }

  /**
   * Provide test data for the class generator test.
   */
  public function classGeneratorDataProvider() {
    return [
      [
        'item_list',
        [
          'item_list' => [
            'variables' => [
              'items' => [],
              'title' => '',
              'list_type' => 'ul',
              'attributes' => [],
              'empty' => NULL
            ],
          ],
        ],
        [
          'items' => 'setItems',
          'title' => 'setTitle',
          'list_type' => 'setListType',
          'attributes' => 'setAttributes',
          'empty' => 'setEmpty',
        ],
        FALSE,
      ],
      [
        'pager',
        [
          'pager' => [
            'render element' => 'pager',
          ],
        ],
        FALSE,
        [
          'pre_render' => 'setPreRender',
          'quantity' => 'setQuantity',
          'element' => 'setElement',
          'parameters' => 'setParameters',
          'tags' => 'setTags',
        ],
      ],
    ];
  }

  /**
   * Get a mock module handler.
   */
  protected function getMockThemeRegistry($theme_definition) {
    $registry = $this->getMockBuilder('Drupal\Core\Theme\Registry')
      ->disableOriginalConstructor()
      ->getMock();
    $registry->expects($this->any())
      ->method('get')
      ->willReturn($theme_definition);
    return $registry;
  }

  /**
   * Get the mock render element manager.
   */
  protected function getMockRenderElementManager() {
    $manager = $this->getMock('Drupal\Core\Render\ElementInfoManagerInterface');
    $manager->expects($this->any())
      ->method('getInfo')
      ->willReturnCallback(function ($theme) {
        return $theme === 'pager' ?
          [
            '#pre_render' => ['Drupal\Core\Render\Element\Pager::preRenderPager'],
            '#theme' => 'pager',
            '#element' => 0,
            '#parameters' => [],
            '#quantity' => 9,
            '#tags' => [],
            '#type' => 'pager',
            '#defaults_loaded' => TRUE,
          ] : [];
      });
    return $manager;
  }
}
