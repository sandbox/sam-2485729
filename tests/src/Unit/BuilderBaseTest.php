<?php

/**
 * @file
 * Contains Drupal\Tests\theme_builder\Unit\BuilderBaseTest.
 */

namespace Drupal\Tests\theme_builder\Unit;

use Drupal\Tests\UnitTestCase;

/**
 * Test the builder base.
 *
 * @group theme_builder
 */
class BuilderBaseTest extends UnitTestCase {

  /**
   * Test that the base renders a content array.
   */
  public function testBaseRendering() {
    $base = $this->getMock('Drupal\theme_builder\BuilderBase', ['drupalRender']);
    $base
      ->expects($this->any())
      ->method('drupalRender')
      ->willReturnCallback(function ($renderable) {
        return $renderable['#content'];
      });

    $base->setRenderable(['#content' => 'Custom Content']);

    // Render returns the renderable array.
    $this->assertEquals($base->render(), ['#content' => 'Custom Content']);
    // Another more verbose and discouraged method is provided for HTML.
    $this->assertEquals($base->getRenderedHtml(), 'Custom Content');
  }

}
