<?php

/**
 * @file
 * Contains Drupal\Tests\theme_builder\Unit\ClassToFilesystemTest.
 */

namespace Drupal\Tests\theme_builder\Unit;

use Drupal\Tests\UnitTestCase;

/**
 * Test saving the classes to the filesystem.
 *
 * @group theme_builder
 */
class ClassToFilesystemTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    define('DRUPAL_ROOT', '/path/to/drupal');
    parent::setUp();
  }

  /**
   * Test saving the builder classes to the filesystem.
   */
  public function testClassToFilesystem() {
    $saver = $this->getMockBuilder('Drupal\theme_builder\ClassToFilesystem')
      ->setConstructorArgs([
        $this->getMockInfoParser(),
        $this->getMockClassGenerator()
      ])
      ->setMethods(['modulePath', 'write', 'fileExists'])
      ->getMock();

    $saver
      ->expects($this->any())
      ->method('modulePath')
      ->willReturn('path/to/module');

    $saver
      ->expects($this->any())
      ->method('fileExists')
      ->willReturn(FALSE);

    $saver
      ->expects($this->exactly(2))
      ->method('write')
      ->willReturnCallback(function ($location, $renderable) {
        $expected_location = '/path/to/drupal/path/to/module/src/ThemeBuilders/' . ($renderable === 'stub' ? 'ItemListBuilder.php' : 'generated/ItemListBuilderBase.php');
        $this->assertEquals($location, $expected_location);
      });

    // Assertions handled in write method above.
    $saver->saveClasses('test_module');
  }

  /**
   * Get a mock info file parser.
   */
  protected function getMockInfoParser() {
    $info_parser = $this->getMock('Drupal\Core\Extension\InfoParserInterface');
    $info_parser
      ->expects($this->any())
      ->method('parse')
      ->willReturn([
        'theme_builders' => [
          'item_list',
        ],
      ]);
    return $info_parser;
  }

  /**
   * A simple mock class generated to help us distinguish the base and stub.
   */
  public function getMockClassGenerator() {
    $generator = $this->getMockBuilder('Drupal\theme_builder\ClassGenerator')
      ->disableOriginalConstructor()
      ->getMock();
    $generator
      ->expects($this->any())
      ->method('generateBase')
      ->willReturn('base');
    $generator
      ->expects($this->any())
      ->method('generateStub')
      ->willReturn('stub');
    return $generator;
  }
}
